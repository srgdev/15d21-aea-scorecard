<?php

/**
 * Class AeaScorecard_LifeCycle
 *
 * Main lifecycle logic of Scorecard plugin
 * Install handles options, actual table creation/updates, versioning, etc.
 *
 * Handles uninstallation, upgrades, overall management of plugin life
 *
 * Relies on LifeCycleIndicator
 */

class AeaScorecard_LifeCycle extends AeaScorecard_InstallIndicator {

    /**
     * Main install function
     * Performs options init, database handling, versioning, and lifecycles
     */
    public function install() {

        // Initialize Plugin Options
        $this->initOptions();

        // Initialize DB Tables used by the plugin
        $this->installDatabaseTables();

        // Other Plugin initialization - for the plugin writer to override as needed
        $this->otherInstall();

        // Record the installed version
        $this->saveInstalledVersion();

        // To avoid running install() more then once
        $this->markAsInstalled();
    }

    /**
     * Uninstalls plugin, marks in WP as so
     */
    public function uninstall() {
        $this->otherUninstall();
        // $this->unInstallDatabaseTables();
        // $this->deleteSavedOptions();
        $this->markAsUnInstalled();
    }

    /**
     * Perform any version-upgrade activities prior to activation (e.g. database changes)
     * @return void
     */
    public function upgrade() {
    }

    /**
     * @return void
     */
    public function activate() {
        
        wp_schedule_event(time() + 60, 'daily', 'update_bills');
    }

    /**
     * @return void
     */
    public function deactivate() {
        // Get the timestamp for the next event.
        $timestamp = wp_next_scheduled( 'update_bills' );
        wp_unschedule_event( $timestamp, 'update_bills');
        $this->uninstall();
    }

    /**
     * @return void
     */
    protected function initOptions() {
    }

    /**
     * @return void
     */
    public function addActionsAndFilters() {
    }

    /**
     
     * @return void
     */
    protected function installDatabaseTables() {
    }

    /**
     
     * @return void
     */
    protected function unInstallDatabaseTables() {
    }

    /**
     * Override to add any additional actions to be done at install time
     * See: http://plugin.michael-simpson.com/?page_id=33
     * @return void
     */
    protected function otherInstall() {
    }

    /**
     * Override to add any additional actions to be done at uninstall time
     * See: http://plugin.michael-simpson.com/?page_id=33
     * @return void
     */
    protected function otherUninstall() {
    }

    /**
     * Includes some important core files for usage
     */
    protected function requireExtraPluginFiles() {
        require_once(ABSPATH . 'wp-includes/pluggable.php');
        require_once(ABSPATH . 'wp-admin/includes/plugin.php');
    }

    /**
     * Slug name for the URL to the Setting page
     * (i.e. the page for setting options)
     * @param $page
     * @return string
     */
    protected function getPageSlug($page) {
        return $this->getSlugBase() ."_". $page;
    }

    /**
     * Returns the stringname of the current class (for slug-style usage)
     * @return string
     */
    protected function getSlugBase(){
        return get_class($this);
    }
    
    /**
     * Wrapper that adds settings menu items/pages to admin menu
     * @return void
     */
    public function addSettingsSubMenuPage() {
        $this->addSettingsSubMenuPageToPluginsMenu();
        //$this->addSettingsSubMenuPageToSettingsMenu();
    }

    /**
     * Adds top level pages/menu items for plugin
     */
    protected function addSettingsSubMenuPageToPluginsMenu() {
        $this->requireExtraPluginFiles();
        $displayName = $this->getPluginDisplayName();
        add_menu_page($displayName, $displayName, 'manage_options', $this->getPageSlug('Settings'), array(&$this, 'settingsPage'), plugin_dir_url( __FILE__ ).'icon.png');
        add_submenu_page($this->getPageSlug('Settings'), 'Members', 'Members', 'manage_options', $this->getPageSlug('members'), array($this, 'manageMembers'));
        add_submenu_page($this->getPageSlug('Settings'), 'Votes', 'Votes', 'manage_options', $this->getPageSlug('manageVotes'), array($this, 'manageVotes'));
        add_submenu_page($this->getPageSlug('Settings'), 'Bills', 'Bills', 'manage_options', $this->getPageSlug('manageBills'), array($this, 'manageBills'));
    }
   
    /**
     * @param  $name string name of a database table
     * @return string input prefixed with the WordPress DB table prefix
     * plus the prefix for this plugin (lower-cased) to avoid table name collisions.
     * The plugin prefix is lower-cases as a best practice that all DB table names are lower case to
     * avoid issues on some platforms
     */
    protected function prefixTableName($name) {
        global $wpdb;
        return $wpdb->prefix .  strtolower($this->prefix($name));
    }

    /**
     * Convenience function for creating AJAX URLs.
     *
     * @param $actionName string the name of the ajax action registered in a call like
     * add_action('wp_ajax_actionName', array(&$this, 'functionName'));
     *     and/or
     * add_action('wp_ajax_nopriv_actionName', array(&$this, 'functionName'));
     *
     * If have an additional parameters to add to the Ajax call, e.g. an "id" parameter,
     * you could call this function and append to the returned string like:
     *    $url = $this->getAjaxUrl('myaction&id=') . urlencode($id);
     * or more complex:
     *    $url = sprintf($this->getAjaxUrl('myaction&id=%s&var2=%s&var3=%s'), urlencode($id), urlencode($var2), urlencode($var3));
     *
     * @return string URL that can be used in a web page to make an Ajax call to $this->functionName
     */
    public function getAjaxUrl($actionName) {
        return admin_url('admin-ajax.php') . '?action=' . $actionName;
    }

}
