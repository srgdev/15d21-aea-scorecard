<div class="wrap">
           
    <h2><?php echo $this->getPluginDisplayName(); echo ' - '; _e($title, 'aea-scorecard'); ?></h2>

    <?php if(isset($this->success)){ ?>
        <div class="updated"><p><?php echo $this->success; ?></p></div>
    <?php } ?>

    <form method="post">
        <input type="hidden" name="page" value="AeaScorecard_Plugin_members" />
        <?php $table = $this->db->prefix . 'scorecard_members'; ?>
        <table class="wp-list-table widefat">
            <tr>
                <td>
                    <?php $senate_members = $this->db->get_results("SELECT * FROM ".$table." WHERE chamber='senate'", OBJECT); ?>
                    <h3>Senate Top Performers</h3>
                    <?php $top_senate = $this->getoption('top_senate'); ?>
                    <?php $top_not_selected = $top_senate == '' ? 5 : (5 - (count($top_senate))); ?>
                    <?php $i = 1; ?>
                    <table class="form-table">
                        <?php if($top_senate) foreach($top_senate as $selected): ?>
                            <tr>
                                <td>
                                    <label for="senate_<?php echo $i; ?>"><?php echo $i; ?></label>
                                </td>
                                <td>
                                    <select name="top_senate[]" id="senate_<?php echo $i; ?>">
                                        <option <?php echo $member->id != $selected ? 'selected' : ''; ?> value="">Select a name</option>
                                        <?php foreach($senate_members as $member): ?>
                                            <option <?php echo $member->id == $selected ? 'selected' : '' ?> value="<?php echo $member->id; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $i ++; ?>
                        <?php endforeach; ?>
                        <?php while($top_not_selected > 0): ?>
                            <tr>
                                <td>
                                    <label for="senate_<?php echo $top_not_selected; ?>"><?php echo $top_not_selected; ?></label>
                                </td>
                                <td>
                                    <select name="top_senate[]" id="senate_<?php echo $top_not_selected; ?>" placeholder="Select a member">
                                        <option selected >Select a name</option>
                                        <?php foreach($senate_members as $member): ?>
                                            <option value="<?php echo $member->id; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $top_not_selected--; ?>
                        <?php endwhile; ?>
                    </table>
                </td>
                <td>
                    <?php $house_members = $this->db->get_results("SELECT * FROM ".$table." WHERE chamber='house'", OBJECT); ?>
                    <h3>House Top Performers</h3>
                    <?php $top_house = $this->getoption('top_house'); ?>
                    <?php $top_not_selected = $top_house == '' ? 5 : (5 - (count($top_house))); ?>
                    <?php $i = 1; ?>
                    <table class="form-table">
                        <?php if($top_house) foreach($top_house as $selected): ?>
                            <tr>
                                <td>
                                    <label for="house_<?php echo $i; ?>"><?php echo $i; ?></label>
                                </td>
                                <td>
                                    <select name="top_house[]" id="house_<?php echo $i; ?>">
                                        <option <?php echo $member->id != $selected ? 'selected' : ''; ?> value="">Select a name</option>
                                        <?php foreach($house_members as $member): ?>
                                            <option <?php echo $member->id == $selected ? 'selected' : '' ?> value="<?php echo $member->id; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $i ++; ?>
                        <?php endforeach; ?>
                        <?php $i = (5 - $top_not_selected + 1); ?>
                        <?php while($i <= 5): ?>
                            <tr>
                                <td>
                                    <label for="house_<?php echo $i; ?>"><?php echo $i; ?></label>
                                </td>
                                <td>
                                    <select name="top_house[]" id="house_<?php echo $i; ?>" placeholder="Select a member">
                                        <option selected="true" value>Select a name</option>
                                        <?php foreach($house_members as $member): ?>
                                            <option value="<?php echo $member->id; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" class="button-primary" value="<?php _e('Save Changes', 'aea-scorecard') ?>"/>
                </td>
            </tr>
        </table>
    </form>

    <br><br>
            
    <form method="post">
        <input type="hidden" name="page" value="AeaScorecard_Plugin_members" />
            <?php 
            $data->search_box('search', 'search_id');
            $data->display();
            ?>
    </form> 
</div>