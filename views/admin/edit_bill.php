<div class="wrap">
           
       
    <h2><?php echo $this->getPluginDisplayName(); echo ' - '; _e($title, 'aea-scorecard'); ?> - <strong><?php echo $data->bill_id; ?></strong> </h2>
    
    <?php if(isset($this->success)){ ?>
        <div class="updated"><p><?php echo $this->success; ?></p></div>
    <?php } ?>
    
    <hr />
    
    <form method="post" action=""> 
        
        <div id="poststuff">

            <div id="post-body" class="metabox-holder columns-2">

                <!-- main content -->
                <div id="post-body-content">

                    <div class="meta-box-sortables ui-sortable">

                        <div class="postbox1">


                            <input type="hidden" name="id" value="<?php echo $data->id; ?>" />
                            <input type="hidden" name="bill" value="<?php echo $data->bill_id; ?>" />
                            <input type="text" name="short_title" value="<?php echo $data->short_title; ?>" id="vote_title" class="input-large" placeholder="Bill Title"/>
                            <br /> <br />
                            
                            <div class="postbox">
                                
                                <h3>Description</h3>
                                <hr />
                                <div class="postInner">
                                    <textarea name="description" class="large-area" id="" ><?php echo $data->description; ?></textarea>
                                </div>
                                
                                <h3>Widget Area</h3>
                                <hr />
                                <div class="postInner">
                                    <textarea name="widget" id="widget" class="large-area" ><?php echo htmlspecialchars_decode($data->widget); ?></textarea>
                                </div>
                                
                            </div>
                                
                        </div>
                    </div>
                </div>



                <!-- sidebar -->
                <div id="postbox-container-1" class="postbox-container">

                    <div class="meta-box-sortables">

                            <div class="postbox">

                                    
                                    <div class="inside">
                                        <table width="100%">                                           
                                            <tr>
                                                <td>
                                                    Bill #  
                                                </td>
                                                <td>
                                                    <?php echo $data->bill_id; ?>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    Congress
                                                </td>
                                                <td>
                                                    <?php echo $data->congress; ?>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    Chamber
                                                </td>
                                                <td>
                                                    <?php echo $data->chamber; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AEA Position
                                                </td>
                                                <td>
                                                    Sponsor <input type="radio" name="position" value="Sponsor" <?php if($data->position == "Sponsor"){ echo "checked"; } ?>  />
                                                    No <input type="radio" name="position" value="Don't Sponsor" <?php if($data->position == "Don\'t Sponsor"){ echo "checked"; } ?> />
                                                </td>
                                            </tr>
                                                
                                        </table>
                                        
                                        
                                        <p class="submit">
                                            <input type="submit" class="button-primary" value="<?php _e('Save Bill', 'aea-scorecard') ?>"/>
                                        </p>
                                    </div> <!-- .inside -->
                                    
                            </div> <!-- .postbox -->

                    </div> <!-- .meta-box-sortables -->

                </div> <!-- #postbox-container-1 .postbox-container -->

            </div> <!-- #post-body .metabox-holder .columns-2 -->            

        </div>       
    </form>                        
                            
    
</div>