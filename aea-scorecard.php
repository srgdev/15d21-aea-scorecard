<?php
/*
Plugin Name: AEA Scorecard
Version: 1.4.6
Author: The Stoneridge Group
Description: Built for the American Energy Alliance, Manages and outputs the AEA Scorecard.  Patched as of 6/10/2015 for NYTimes API Failures.
Text Domain: aea-scorecard
Bitbucket Plugin URI: https://bitbucket.org/srgdev/15d21-aea-scorecard
Bitbucket Branch: master
*/

$AeaScorecard_minimalRequiredPhpVersion = '5.0';

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function AeaScorecard_noticePhpVersionWrong() {
    global $AeaScorecard_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "AEA Scorecard" requires a newer version of PHP to be running.',  'aea-scorecard').
            '<br/>' . __('Minimal version of PHP required: ', 'aea-scorecard') . '<strong>' . $AeaScorecard_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'aea-scorecard') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}

function AeaScorecard_PhpVersionCheck() {
    global $AeaScorecard_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $AeaScorecard_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'AeaScorecard_noticePhpVersionWrong');
        return false;
    }
    return true;
}

function AeaScorecard_enqueue(){
    // Enqueue all globally necessary stylesheets
    wp_enqueue_style('scorecard-fonts', "http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic");
    wp_enqueue_style('scorecard-font-awesome', "http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css") ;
    wp_enqueue_style('aealayout', plugins_url('/front-end/css/layout.css', __FILE__));
    wp_enqueue_style('aearesponsive', plugins_url('/front-end/css/responsive.css', __FILE__));
    wp_enqueue_style('selectyze', plugins_url('/front-end/css/selectyze.jquery.css', __FILE__));

    // Enqueue all globally necessary front end scripts
    wp_enqueue_script('stickyheader', plugins_url('/front-end/js/jquery.stickyheader.min.js', __FILE__),array('jquery'));
    wp_enqueue_script('scrolltotop', plugins_url('/front-end/js/jquery.scrollToTop.min.js', __FILE__),array('jquery'));
    wp_enqueue_script('tablesorter', plugins_url('/front-end/js/jquery.tablesorter.min.js', __FILE__),array('jquery'));
    wp_enqueue_script('parsley', plugins_url('/front-end/js/parsley-IEshim.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('selectyze', plugins_url('/front-end/js/selectyze.jquery.min.js', __FILE__), array('jquery'), true,true);
    wp_enqueue_script('aeamain', plugins_url('/front-end/js/main.min.js', __FILE__), array('jquery'));
    wp_localize_script('aeamain', 'aea_ajax_url', plugin_dir_url( __FILE__ ).'front-end/front_end_ajax.php');
}
add_action('init', 'AeaScorecard_enqueue');

/**
 * Next, run the version check.
 * If it is successful, include all the plugin files and
 * continue with initialization for this plugin
 */
if (AeaScorecard_PhpVersionCheck()) {

    // Require main plugin files
    include_once('AeaScorecard_actions.php');
    include_once('AeaScorecard_admin_pages.php');
    include_once('AeaScorecard_OptionsManager.php');
    include_once('AeaScorecard_InstallIndicator.php');
    include_once('AeaScorecard_LifeCycle.php');
    require_once('AeaScorecard_Plugin.php');

    // Only load and run the init function if we know PHP version can parse it
    include_once('aea-scorecard_init.php');

    // Run initialization
    AeaScorecard_init(__FILE__);
}