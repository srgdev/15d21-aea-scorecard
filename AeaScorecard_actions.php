<?php

class AeaScorecard_actions {

    // Ny times API URL
    private $nyApiLocation = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/";

    // Sunlight Labs API URL
    private $sunlightApi = "http://congress.api.sunlightfoundation.com/";

    // Wrapper for $wpdb
    protected $db;
    
    public function __construct(){
        global $wpdb;
        $this->db = $wpdb;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Sync members from given congress from api's
     * @param string $congress
     * @param string $chamber
     */
    public function membersSync($congress = "114", $chamber = "senate"){

        // Set a big timeout, this API tends to take a while (5 mins)
        ini_set('default_socket_timeout', 300);
        set_time_limit(400);//

        $this->session = $congress;

        // Prepare URL to get members information
        //$path = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/".$congress.'/'.$chamber."/members.json?api-key=".$this->getOption('nyapikey');
        $path = "http://congress.api.sunlightfoundation.com/legislators?per_page=all&chamber=".$chamber."&apikey=".$this->getOption('sunlightkey');

        // Get data
        $ny_res = file_get_contents($path);
        // create curl resource
        // $ch = curl_init();
        // // set url
        // curl_setopt($ch, CURLOPT_URL, $path);
        // //return the transfer as a string
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
        // curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds
        // // $output contains the output string
        // $ny_res = curl_exec($ch);
        // // close curl resource to free up system resources
        // curl_close($ch);

        if(!$ny_res){
            echo '<div class="error"><p>Sunlight API unreachable.</p></div>';
            return false;
        }
        // JSON decode the data from filestream
        $data = json_decode($ny_res,true);

        // Parse results into separate vars
        // $results = $data['results'][0]['members'];
        // $session = $data['results'][0]['congress'];
        $results = $data['results'];
        $session = $congress;

        // Loop through members
        foreach($results as $m){

            // Assign state
            $state = $m['state'];
            
            //Filter out no included states
            if($state != "MP" &&$state != "GU" && $state != "AS" && $state != "PR" && $state != "VI" && $state != "FM" && $state != "MH" && $state != "PW" && $state != "CZ" && $state != "PI" && $state != "TT" && $state != "CM" ) {
               
                // Get Member Data from Sulight Labs
                $sunlightPath = "http://congress.api.sunlightfoundation.com/legislators?bioguide_id=".$m['bioguide_id']."&apikey=".$this->getOption('sunlightkey');

                // Get data
                $sunlight_res = file_get_contents($sunlightPath);

                if(!$sunlight_res){
                    echo '<div class="error"><p><Sunlight API unreachable: '.$sunlightPath.'</p></div>';
                    return false;
                    break;
                }
                // JSON decode data from filestream
                $memberData = json_decode($sunlight_res, true);

                // Get result array
                $memberResults = $memberData['results'][0];

                // If there is a result
                if($memberResults){

                    // Assign Government ID
                    $govtrack_id = $memberResults['govtrack_id'];
                    //Get The Image
                    $image_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                    
                } else { // If we didn't get any data

                    // Assign empty
                    $govtrack_id = "";
                    $image_path = "";
                }
                
                // Prepare rest of data
                $district = (isset($m['district'])) ? $m['district'] : "";
                $title = ($chamber == "senate") ? "Sen." : "Rep." ;
                $member = $m['bioguide_id'];

                //Build the Data Array
                $data = array(
                    'fName' => $m['first_name'],
                    'mname' => $m['middle_name'],
                    'lName' => $m['last_name'],
                    'congID' => $member,
                    'govTrackID' => $govtrack_id,
                    'congress' => $session,
                    'state' => $m['state'],
                    'party' => $m['party'],
                    'district' => $district,
                    'chamber' => $chamber,
                    'gender' => "",
                    'bio' => "",
                    'title' => $title,
                    'image_path' => $image_path
                );

                // Select table
                $table = $this->db->prefix . 'scorecard_members';

                // Check for existing entries
                $rowCount = $this->db->get_results("SELECT * FROM $table WHERE congID = '$member' AND congress = '$session'");

                // If there is no entry for this member
                if(count($rowCount ) == 0){

                    // Create new member
                    $this->db->insert( $table, $data );
                } else { // Member exists

                    // Update entry
                    $this->db->update( $table, $data, array('congID' => $member, 'congress' => $session) );
                }

                // Prepare overwrites
                $overtable = $this->db->prefix . 'scorecard_members_overwrite';

                // Check if entry exists in overwrites
                $overCount = $this->db->get_results("SELECT * FROM $overtable WHERE congID = '$member' AND congress = '$session'");

                // If an entry exists
                if(count($overCount ) != 0){

                    // Prep data for overwrite
                    $overdata = array(
                        'fName' => $overCount[0]->fName,
                        'lName' => $overCount[0]->lName
                    );

                    // Update overwrite table
                    $this->db->update( $table, $overdata, array('congID' => $member, 'congress' => $session) );

                }
            }
        }

        // Recalculate scores once all data has been parsed
        $this->calc_scores($congress);

        return true;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a single member
    * @param $id    string
    * @return array
    */
    public function getMember($id){
        $table = $this->dbPrefix('scorecard_members');
        $member = $this->db->get_results("SELECT * FROM $table WHERE id = '$id'");
        return $member;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a all members
    * @param $congress    string
    * @return array
    */
    public function getMembers($congress){
        $table = $this->dbPrefix('scorecard_members');
        $member = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress'");
        return $member;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * overwrites member Data to member table and member_overwrites table
     * @param $congId
     * @param $congress
     * @param $data
     */
    public function overwriteMember($congId, $congress, $data){
        $table = $this->db->prefix . 'scorecard_members_overwrite';
        $rowCount = $this->db->get_results("SELECT * FROM $table WHERE congID = '$congId' AND congress = '$congress'");
        if(count($rowCount ) == 0){
            $this->db->insert( $table, $data );
        }else{
            $this->db->update( $table, $data, array('congID' => $congId, 'congress' => $congress) );
        }
        
        $table = $this->db->prefix . 'scorecard_members';
        $this->db->update( $table, $data, array('congID' => $congId, 'congress' => $congress) );
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * adds a vote from api
     * @param $roll_call
     * @return mixed
     */
    public function addVote($roll_call){

        // Prepare data query
        $fields = 'voters,result,number,chamber,year,congress,voted_at,question,bill_id';
        $url = $this->sunlightApi.'votes?roll_id='.$roll_call.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');

        $sunlight_res = file_get_contents($url);

        if(!$sunlight_res){
            return false;
        }

        // Get data
        $votes = json_decode($sunlight_res, true);
        $voteData = $votes['results'];

        // Prep entry to database
        $billid = (isset($voteData[0]['bill_id'])) ? $voteData[0]['bill_id'] : '';
        $data = array(
            'roll_call' => $roll_call,
            'bill_id' => $billid,
            'chamber' => $voteData[0]['chamber'],
            'congress' => $voteData[0]['congress'],
            'result' => $voteData[0]['result'],
            'vote_date' => $voteData[0]['voted_at'],
            'question' => $voteData[0]['question']
        );

        // Add entry
        $table = $this->db->prefix . 'scorecard_key_votes';
        $this->db->insert( $table, $data );
        $lastid = $this->db->get_col("SELECT ID FROM $table ORDER BY ID DESC LIMIT 0 , 1");

        // Prepare yes/no count
        $count_yes = 0;
        $count_no = 0;

        // Prepare list of entries for individual members
        foreach($voteData[0]['voters'] as $v) {

            // Parse vote data
            $voter = $v['voter']['bioguide_id'];
            $vote = $v['vote'];
            if($vote == "Yea") {$vote = "Yes"; $count_yes++;}
            if($vote == "Nay") {$vote = "No"; $count_no++;}

            // Prep insert array
             $data1 = array(
                'vote_id' => $roll_call,
                'member' => $voter,
                'vote' => $vote,
                'congress' => $voteData[0]['congress']
            );

            // Save vote info
            $table = $this->db->prefix . 'scorecard_votes';
            $this->db->insert( $table, $data1 );
        }

        // Update yes/no counts
        $table = $this->db->prefix . 'scorecard_key_votes';
        $this->db->update( $table, array('count_yes' => $count_yes, 'count_no' => $count_no), array('id' => $lastid[0]));

        // Recalculate scores
        $this->calc_scores($voteData[0]['congress']);

        // return lastid;
        return $lastid[0];

    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a single vote
    * @param $id  string
    * @return array
    */
    public function getVote($id){
        $table = $this->dbPrefix('scorecard_key_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE id = '$id' ");
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a all votes
    * @param $congress  string
    * @return array
    */
    public function getVotes($congress){
        $table = $this->dbPrefix('scorecard_key_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' ");
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a member voting positions on all votes
    * @param $congress  string
    * @param $member    string
    * @return array
    */
    public function getMemberVotes($congress,$member){
        $table = $this->dbPrefix('scorecard_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' AND member = '$member' ");
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Returns a member voting positions on single votes
    * @param $congress  string
    * @param $member    string
    * @param $vote      string
    * @return array
    */
    public function getMemberVote($congress,$member, $vote){
        $table = $this->dbPrefix('scorecard_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' AND member = '$member'  AND vote_id = '$vote' ");
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Add Bill from API
     * @param $bill_id string
     * @return mixed
     */
    public function addBill($bill_id){
        $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
        $url = $this->sunlightApi.'bills?bill_id='.$bill_id.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');

        $sunlight_res = file_get_contents($url);

        if(!$sunlight_res){
            return false;
        }

        $bill = json_decode($sunlight_res, true);
        $billData = $bill['results'][0];

        if($billData['bill_type'] == 'hr' || $billData['bill_type'] == 'hltr'){
            $chamber = 'house';
        }else{
            $chamber = 'senate';
        }
        $data = array(
            'bill_id' => $bill_id,
            'billNumber' => $billData['number'],
            'congress' => $billData['congress'],
            'billType' => $billData['bill_type'],
            'short_title' => $billData['short_title'],
            'official_title' => $billData['official_title'],
            'introduced_date' => $billData['introduced_on'],
            'chamber' => $chamber
        );


        $table = $this->db->prefix . 'scorecard_bills';
        $id = $this->db->insert( $table, $data );
        $lastid = $this->db->get_col("SELECT ID FROM $table ORDER BY ID DESC LIMIT 0 , 1" );
        
        $congress = $billData['congress'];
        foreach($billData['cosponsor_ids'] as $s){
            $data1 = array(
                'bill_id' => $bill_id,
                'member' => $s,
                'congress' => $congress
            );
            
            $table = $this->db->prefix . 'scorecard_sponsors';
            $this->db->insert( $table, $data1 );
        }
        
        
        $data2 = array(
            'bill_id' => $bill_id,
            'member' => $billData['sponsor_id'],
            'congress' => $congress
        );
        
        $table = $this->db->prefix . 'scorecard_sponsors';
        $this->db->insert( $table, $data2);
        $this->calc_scores($congress);
        return $lastid[0];
        
    }

    /*----------------------------------------------------------------------------------------------------*/
    
    /**
    * Returns a single bill
    *
    * @param $id  string
    *
    * @return array
    */
    public function getBill($id){
        $table = $this->dbPrefix('scorecard_bills');
        $results = $this->db->get_results("SELECT * FROM $table WHERE id = '$id' ");
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Returns all bills
     * @param $congress
     * @param $chamber
     * @return mixed
     */
    public function getBills($congress, $chamber){
        $table = $this->dbPrefix('scorecard_bills');
        if($chamber == "house"){
            $results = $this->db->get_results("SELECT * FROM $table WHERE (billType = 'hr' or billType = 'hltr') AND congress = '$congress' ");
        }else{
            $results = $this->db->get_results("SELECT * FROM $table WHERE (billType = 's' or billType = 'sltr') AND congress = '$congress' ");
        }
        return $results;
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Recaluclate member scores
    *
    * @param $congress     string
    *
    * @return void
    */
    public function calc_scores($congress){

        $members = $this->getMembers($congress);
        $houseAvg = 0;
        $senAvg = 0;
        $houseTotal = 0;
        $senTotal = 0;

        foreach($members as $member){

            $voteScore = $this->vote_score($member->congID,$member->chamber,$congress);
            $billScore = $this->bill_score_sec($member->congID,$member->chamber,$congress);

            $count_bills = $billScore['count'];
            $billScore = $billScore['score'];

            $vote_weight = .90;
            $bill_weight = .10;

            if($count_bills > 5){
                $bill_weight = .15;
            } else {
                $bill_weight = round($count_bills * .03, 2);
            }

            $vote_weight = round(1 - $bill_weight, 2);

            $score = round(($voteScore*$vote_weight) + ($billScore*$bill_weight), 2);



            $table = $this->dbPrefix('scorecard_members');
            $this->db->update( $table, array('score' => round($score * 100)), array('congID' => $member->congID, 'congress' => $congress) );

            if($member->chamber == 'house'){
                $houseTotal++;
                $houseAvg = $houseAvg + $score;
            }else{
                $senTotal++;
                $senAvg = $senAvg + $score;
            }
        }

        $houseAvg = round(($houseAvg / $houseTotal) * 100);
        $this->updateOption('houseAvg', $houseAvg);



        $senAvg = round(($senAvg / $senTotal) * 100);
        $this->updateOption('senAvg', $senAvg);

        $this->clean_bills_table();

    }


    /*----------------------------------------------------------------------------------------------------*/

    /**
    * Calculates the vote score for a given member
    *
    * @param $member    string
    * @param $chamber   string
    * @param $congress  string
    *
    * @return num
    */
    private function vote_score($member,$chamber,$congress){
        $votes = $this->getVotes($congress);
        $votesCast = $this->getMemberVotes($congress,$member);
        $votesCast = count($votesCast);

        $i = 0;
        $v = 0;
        foreach($votes as $vote){
            $id = $vote->roll_call;
            $pref = $vote->position;
            $mvote = $this->getMemberVote($congress,$member,$id);
            $vote = $mvote[0]->vote;
            if($pref == $vote){ $i++; }
            if($pref == null || $pref == "" || $vote == "Not Voting" || $vote == "Present" || $vote == "present") {if($votesCast) {$votesCast = $votesCast-1;}}
            $v++;
        }
        $votesRight =  $i;
        if($votesCast != 0 && $votesCast != "") {
            $rawVoteScore =  $votesRight / $votesCast;
        }else{
            $rawVoteScore = 0;
        }
        return $rawVoteScore;
    }

    /*----------------------------------------------------------------------------------------------------*/
    
    /**
    * Calculates the Bill score for a given member
    *
    * @param $member    string
    * @param $chamber   string
    * @param $congress  string
    *
    * @return num
    */
    private function bill_score($member,$chamber,$congress){
        $bills = $this->getBills($congress,$chamber);
        $sponsorOp = count($bills);
        
        $j = 0;
        foreach($bills as $bill){
            $rowid = $bill->id;
            $id = $bill->bill_id;
            $pref = $bill->position;
            $bill = $this->getBill($rowid);
            $table = $this->dbPrefix('scorecard_sponsors');
            $results = $this->db->get_results("SELECT * FROM $table WHERE bill_id = '$id' AND member = '$member' AND congress = '$congress'");
            $count = count($results);
            if($count != 0 && $pref == 'Sponsor'){ $j++; }
        }
        $sonposrRight = $j;
        
        if($sponsorOp != 0 && $sponsorOp != "") {
            $rawSponsor =  $sonposrRight/$sponsorOp;
        }else{
            $rawSponsor =  0;
        }
        return array('score' => $rawSponsor, 'count' => count($bills));
    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Calulates bill score for given member and congress
     * @param $member
     * @param $chamber
     * @param $congress
     * @return array
     */
    private function bill_score_sec($member, $chamber, $congress){

        // Get member's cosponsorships
        $table = $this->dbPrefix('scorecard_sponsors');
        $member_bills = $this->db->get_results("SELECT bill_id FROM $table WHERE member = '$member' AND congress = '$congress'");

        // Get AEA's supported bills
        $table = $this->dbPrefix('scorecard_bills');
        $aea_bills = $this->db->get_results("SELECT bill_id FROM $table WHERE position='Sponsor' AND congress='$congress' AND chamber='$chamber'");

        $should_sponsor = array();
        foreach($aea_bills as $bill){
            $should_sponsor[] = $bill->bill_id;
        }

        

        // Run a counter
        $bills_count = count($aea_bills);

        // Start tracking how many were backed by AEA
        $did_sponsor_correct = 0;

        if(count($member_bills) > 0){
            foreach($member_bills as $bill){

                if(in_array($bill->bill_id, $should_sponsor)){
                    $did_sponsor_correct++;
                }
            }

            return array('score' => $did_sponsor_correct/$bills_count, 'count' => $bills_count);

        } else {
            return array('score' => 0, 'count' => $bills_count);
        }

    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Removes orphan entries from the bills table
     */
    private function clean_bills_table(){
        $table = $this->dbPrefix('scorecard_sponsors');
        $active_bills = $this->db->get_results("SELECT DISTINCT bill_id FROM $table");

        $bill_array = array();

        foreach($active_bills as $bill){
            $id = $bill->bill_id;
            $bill_array[] = $id;
        }

        $bill_array = implode("', '", $bill_array);

        $table = $this->dbPrefix('scorecard_bills');
            $this->db->query("DELETE FROM $table WHERE bill_id NOT IN ('$bill_array')");

    }

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Returns tablename prefix for WP
     * @param $table
     * @return string
     */
    private function dbPrefix($table){
        return $this->db->prefix . $table;
    }

    /*----------------------------------------------------------------------------------------------------*/

    public function update_bill($id){

        // Set up returns
        $new_sponsors = array();

        // Get bill number
        $table = $this->dbPrefix('scorecard_bills');
        $bill = $this->db->get_results("SELECT bill_id FROM $table WHERE id='$id'");
        $bill_id = $bill[0]->bill_id;

        // Query sunlight
        $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
        $url = $this->sunlightApi.'bills?bill_id='.$bill_id.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');

        $sunlight_res = file_get_contents($url);

        if(!$sunlight_res){
            return false;
        }

        $bill = json_decode($sunlight_res, true);
        $billData = $bill['results'][0];
        $congress = $billData['congress'];

        // Remove entries associated with bill's cosponsors
        $table = $this->dbPrefix('scorecard_sponsors');
        $sponsors = $this->db->get_results("SELECT member FROM $table WHERE bill_id='$bill_id'");
        $old_sponsors = array();
        foreach($sponsors as $sponsor){
            $old_sponsors[] = $sponsor->member;
        }

        $this->db->delete($table, array('bill_id' => $bill_id));

        // loop through cosponsors in each house
        $members_table = $this->dbPrefix('scorecard_members');
        foreach($billData['cosponsor_ids'] as $s){
            $data1 = array(
                'bill_id' => $bill_id,
                'member' => $s,
                'congress' => $congress
            );

            if(!in_array($s, $old_sponsors)){
                $member = $this->db->get_results("SELECT fName, lName FROM $members_table WHERE congID='$s'");
                $new_sponsors[] = $member[0]->fName . ' ' . $member[0]->lName;
            }

            // Add co sponsor
            $table = $this->db->prefix . 'scorecard_sponsors';
            $this->db->insert( $table, $data1 );
        }


        $data2 = array(
            'bill_id' => $bill_id,
            'member' => $billData['sponsor_id'],
            'congress' => $congress
        );
        if(!in_array($billData['sponsor_id'], $old_sponsors)){
            $id = $billData['sponsor_id'];
            $member = $this->db->get_results("SELECT fName, lName FROM $members_table WHERE congID='$id'");
            $new_sponsors[] = $member[0]->fName . ' ' . $member[0]->lName;
        }

        // Add main sponsor
        $table = $this->db->prefix . 'scorecard_sponsors';
        $this->db->insert( $table, $data2);

        // Calc scores
        $this->calc_scores($congress);
        return implode(', ', $new_sponsors);
    }

    /*----------------------------------------------------------------------------------------------------*/

    public function update_bills(){
        $table = $this->dbPrefix('scorecard_bills');
        $bills = $this->db->get_results("SELECT id, bill_id, congress FROM $table");
        foreach($bills as $bill){

            $bill_id = $bill->bill_id;

            // Query sunlight
            $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
            $url = $this->sunlightApi.'bills?bill_id='.$bill_id.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');

            $sunlight_res = file_get_contents($url);

            if(!$sunlight_res){
                continue;
            }

            $bill = json_decode($sunlight_res, true);
            $billData = $bill['results'][0];
            $congress = $billData['congress'];

            // Remove entries associated with bill's cosponsors
            $table = $this->dbPrefix('scorecard_sponsors');
            $this->db->delete($table, array('bill_id' => $bill_id));

            // loop through cosponsors in each house
            $members_table = $this->dbPrefix('scorecard_members');
            foreach($billData['cosponsor_ids'] as $s){
                $data1 = array(
                    'bill_id' => $bill_id,
                    'member' => $s,
                    'congress' => $congress
                );

                // Add co sponsor
                $table = $this->db->prefix . 'scorecard_sponsors';
                $this->db->insert( $table, $data1 );
            }


            $data2 = array(
                'bill_id' => $bill_id,
                'member' => $billData['sponsor_id'],
                'congress' => $congress
            );

            // Add main sponsor
            $table = $this->db->prefix . 'scorecard_sponsors';
            $this->db->insert( $table, $data2);

            // Calc scores
            $this->calc_scores($congress);

            error_log('Bill '.$bill_id.' updated.');
        }
    }

    /*----------------------------------------------------------------------------------------------------*/

}

