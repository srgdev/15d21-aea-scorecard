<?php

/**
 * Class AeaScorecard_admin_pages
 *
 * Controls routing of form submissions, data management, and admin pages contruction
 * based on called routes
 */

class AeaScorecard_admin_pages extends AeaScorecard_actions{

    /**
    * Creates HTML for the Administration page to set options for this plugin.
    * @return void
    */
    public function settingsPage() {

        // Set current pagename
        $this->page = 'settings';

        // Check permissions
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Save Settings Menu
        if(isset($_POST['current_congress'])){
            $this->updateOption('current_congress', $_POST['current_congress']);
            $this->updateOption('excerpt_length', $_POST['excerpt_length']);
            $this->success = "Your Scorecard settings have been saved!";
        }

        // Save sessions menu
        if(isset($_POST['new_session_number'])){

            // Get current sessions, or empty array
            $sessions = ($this->getOption('sessions')) ? $this->getOption('sessions') : array();

            // Set up new data
            $data = array(
                'number' => $_POST['new_session_number'],
                'years' => $_POST['new_session_yearOne']." - ".$_POST['new_session_yearTwo']
            );

            // Push new session data, then sort according to session #
            array_push($sessions,$data);
            usort($sessions, function($a, $b) {
                return $a['number'] - $b['number'];
            });
            $sessions = array_reverse($sessions);

            // Update new sessions array option
            $this->updateOption('sessions', $sessions);
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            $this->success = "Session #".$data['number']." has been Added";
            $this->page = 'sessions';
        }
        
        // Delete Session of congress
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            if($_GET['source'] == 'session' && !isset($_POST['new_session_number'])){

                // Get session #
                $val = $_GET['value'];

                // Get sessions array
                $sessions = $this->getOption('sessions');

                // Loop through, find session to delete, remove it
                foreach($sessions as $subKey => $subArray){
                    if($subArray['number'] == $val){
                        unset($sessions[$subKey]);
                    }
                }

                // Update new sessions array
                $this->updateOption('sessions', $sessions);
                $this->updateOption('last_updated', date('Y-m-d H:i:s'));
                $this->success = "Session #".$val." has been deleted";
                $this->page = 'sessions';
            }
        }

        // Create and save API menu
        $optionMetaData = $this->getOptionMetaData();
        if(isset($_POST['nyapikey'])){
            if ($optionMetaData != null) {
                foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                    if (isset($_POST[$aOptionKey])) {
                        $this->updateOption($aOptionKey, $_POST[$aOptionKey]);
                    }
                }
                $this->page = 'api';
                $this->success = "Your API settings have been saved!";
            }
        }

        // Save content menu
        if(isset($_POST['homepage_intro'])){
            if ($optionMetaData != null) {
                $this->updateOption('homepage_intro', $_POST['homepage_intro']);
                $this->page = 'content';
                $this->success = "Your Content settings have been saved!";
            }
        }

        // Save about
        if(isset($_POST['about_text'])){
            if ($optionMetaData != null) {
                $this->updateOption('about_text', $_POST['about_text']);
                $this->page = 'content';
                $this->success = "Your Content settings have been saved!";
            }
        }

        // Save about
        if(isset($_POST['about_page'])){
            if ($optionMetaData != null) {
                $this->updateOption('about_page', $_POST['about_page']);
                $this->page = 'content';
                $this->success = "Your Content settings have been saved!";
            }
        }

        // Save "Take Action" link
        if(isset($_POST['take_action_link'])){
            if ($optionMetaData != null) {
                $this->updateOption('take_action_link', $_POST['take_action_link']);
                $this->page = 'content';
                $this->success = "Your Content settings have been saved!";
            }
        }

        // Sync Members
        if(isset($_POST['syncmembers']) && isset($_POST['syncchamber'])){
            $this->page = 'data';
            $chamber = $_POST['syncchamber'];
            $results = $this->membersSync($this->getOption('current_congress'), $chamber);
            if(!$results){
                $this->error = "Could not reach APIs for ".ucfirst($chamber)." at this time. Please try again at a different time to ensure data integrity.";
            } else {
                $this->success = ucfirst($chamber)." Data has been synced!";
                $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            }
        }
        
        //Calc Scores
        if(isset($_POST['calcscores'])){
           $this->page = 'data';
           $this->calc_scores($this->getOption('current_congress'));
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
           $this->success = "Scores have be Recalculated!";
        }

        // HTML for the page
        $this->build('admin/settings_page', 'Settings');
    }

    /**
    * Manage Members page; controls list/edit tables
    * @return void
    */
    public function manageMembers(){

        // Check permissions
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }

//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';

        // Save top performers
        if(isset($_POST['top_house'])){

            $this->updateOption('top_house', $_POST['top_house']);
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            $this->success = "Member Data has been Saved!";
        }
        if(isset($_POST['top_senate'])){
            $this->updateOption('top_senate', $_POST['top_senate']);
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            $this->success = "Member Data has been Saved!";
        }
        
        // Save member edit page
        if(isset($_POST['memberPost'])){

            // Construct new data
            $data = array(
                'fName' => $_POST['fName'],
                'lName' => $_POST['lName'],
                'congID' => $_POST['congID'],
                'congress' => $_POST['congress'],
                'image_path' => $_POST['image_path']
            );

            // Add to overwrites table
            $this->overwriteMember($_POST['congID'], $_POST['congress'], $data);
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            $this->success = "Member Data has been Saved!";
        }

        // Create member edit view
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){

            // Get requested member
            $member = $this->getMember($_GET['id']);

            // HTML for the page
            $this->build('admin/edit_member', 'Edit Member',$member[0]);

        } else{ // Create member List view

            // Include table creation template
            require_once('includes/AeaScorecard_create_table.php');

            // Construct tabular list of members
            $table = $this->db->prefix . 'scorecard_members';
            $listTable = new AeaScorecard_create_table();
            if($listTable->has_data($table)){
                $listTable->prepare_items($table , array('congID', 'state', 'fName', 'lName', 'chamber', 'congress', 'score') , 20 , array('state','lName', 'chamber', 'congress', 'score') , false, true);    
            } else {
                $listTable = null;
            }

            // HTML for the page
            $this->build('admin/members_page', 'Members' , $listTable);
        }
    }

    /**
    * Manage Votes Page
    * Controls List/Create/Edit/Delete
    * @return void
    */
    public function manageVotes(){

        // Check permissions
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Add vote
        if( isset($_POST['roll_call']) ){
            $vote = $this->addVote( $_POST['roll_call'] );
            if(!$vote){
                $this->error = "Could not reach Sunlight API.";
                require_once('includes/AeaScorecard_create_table.php');
                $table = $this->db->prefix . 'scorecard_key_votes';
                $listTable = new AeaScorecard_create_table();
                $listTable->prepare_items($table , array( 'roll_call',  'congress', 'chamber') , 20 , "" , true, true);
                // HTML for the page
                $this->build('admin/votes_page', 'Key Votes', $listTable );
            } else {
                $this->success = "Vote ".$_POST['roll_call']." has been added";
                $this->updateOption('last_updated', date('Y-m-d H:i:s'));
                $data = $this->getVote($vote);
                $this->build( 'admin/edit_vote', 'Edit Key Vote',  $data[0]);
            }
        }
        
        // Save Vote
        if( isset($_POST['vote_title']) ){            
            $data = array(
                'vote_title' => $_POST['vote_title'],
                'description' => $_POST['description'],
                'widget' => htmlspecialchars(stripslashes($_POST['widget'])),
                'position' => $_POST['position']
            );
            $id = $_POST['id'];
            $table = $this->db->prefix . 'scorecard_key_votes';
            $this->db->update( $table, $data, array('id' => $id) );
            $this->calc_scores($this->getOption('current_congress'));
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
            $this->success = "Vote ".$_POST['vote']." has been updated";
        }

        // Delete Vote
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            $id = $_GET['id'];
            $table = $this->db->prefix . 'scorecard_key_votes';

            // Get vote id, then remove vote entry
            $vote_id = $this->db->get_var("SELECT roll_call FROM $table WHERE id = '$id'");
            $this->db->query("DELETE FROM $table WHERE id = '$id'");

            // Remove individual votes so we dont have copies of same votes (where same vote is added and deleted)
            $table = $this->db->prefix . 'scorecard_votes';
            $this->db->query("DELETE FROM $table WHERE vote_id = '$vote_id'");

            $this->calc_scores($this->getOption('current_congress'));

            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
        }

        // Create vote edit view
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){
            $id = $_GET['id'];
            $vote = $this->getVote($id);
            $this->build('admin/edit_vote', 'Edit Key Vote', $vote[0] );
        }
        
        // Create Vote list view
        elseif(!isset($_POST['roll_call'])){
            require_once('includes/AeaScorecard_create_table.php');
            $table = $this->db->prefix . 'scorecard_key_votes';
            $listTable = new AeaScorecard_create_table();
            $listTable->prepare_items($table , array( 'roll_call',  'congress', 'chamber') , 20 , "" , true, true);
            // HTML for the page
            $this->build('admin/votes_page', 'Key Votes', $listTable );
        }
    }

    /**
    * Manage Bills Page
    * Controls List/Create/Edit/Delete
    * @return void
    */
    public function manageBills(){

        // Check permissions
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Add bill
        if( isset($_POST['bill_num']) ){
            $bill = $this->addBill( $_POST['bill_num'] );
            if(!$bill){
                $this->error = "Could not reach Sunlight API";
                require_once('includes/AeaScorecard_create_table.php');
                $table = $this->db->prefix . 'scorecard_bills';
                $listTable = new AeaScorecard_create_table();
                $listTable->prepare_items($table , array('bill_id', 'short_title', 'congress') , 20 , "" , true, true);
                // HTML for the page
                $this->build('admin/bills_page', 'Bills', $listTable );
            } else {
                $this->success = "Bill ".$_POST['bill_num']." has been added";
                $data = $this->getBill($bill);
                $this->updateOption('last_updated', date('Y-m-d H:i:s'));
                $this->build( 'admin/edit_bill', 'Edit Bill',  $data[0]);
            }
        }
        
        // Save bill
        if( isset($_POST['short_title']) ){            
            $data = array(
                'short_title' => $_POST['short_title'],
                'description' => $_POST['description'],
                'widget' => htmlspecialchars(stripslashes($_POST['widget'])),
                'position' => $_POST['position']
            );
            $id = $_POST['id'];
            $table = $this->db->prefix . 'scorecard_bills';
            $this->db->update( $table, $data, array('id' => $id) );
            $this->updateOption('last_updated', date('Y-m-d H:i:s'));

            $renewed_sponsors = $this->update_bill($id);
            if($renewed_sponsors){
                $this->success = "Bill ".$_POST['bill']." has been updated along with sponsorships: ".$renewed_sponsors;
            } else {
                $this->success = "Bill ".$_POST['bill']." has been updated";
            }
        }

        // Delete Vote
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            $id = $_GET['id'];
            $table = $this->db->prefix . 'scorecard_bills';

            $bill = $this->db->get_results("SELECT bill_id FROM $table WHERE id = '$id'");
            $bill = $bill[0];
            $bill_id = $bill->bill_id;

            $this->db->query("DELETE FROM $table WHERE id = '$id'");

            $table = $this->db->prefix . 'scorecard_sponsors';
            $this->db->query("DELETE FROM $table WHERE bill_id = '$bill_id'");

            $this->calc_scores($this->getOption('current_congress'));

            $this->updateOption('last_updated', date('Y-m-d H:i:s'));
        }

        // Create Bill Edit View
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){
            $id = $_GET['id'];
            $bill = $this->getBill($id);
            // HTML for the page
            $this->build('admin/edit_bill', 'Edit Bill', $bill[0] );

        }
        
        // Create Bill List View
        elseif(!isset($_POST['bill_num']) && !isset($_POST['short_title'])){
            require_once('includes/AeaScorecard_create_table.php');
            $table = $this->db->prefix . 'scorecard_bills';
            $listTable = new AeaScorecard_create_table();
            $listTable->prepare_items($table , array('bill_id', 'short_title', 'congress') , 20 , "" , true, true);
            // HTML for the page
            $this->build('admin/bills_page', 'Bills', $listTable );
        }
    }
}
