jQuery( document ).ready(function() {
	
	jQuery('.settingTabs a').click(function(){
		jQuery('.settingTabs a').removeClass('nav-tab-active');
		jQuery(this).addClass('nav-tab-active');
		var tab = jQuery(this).data('tab');
		jQuery('.tabpage').removeClass('active');
		jQuery('.'+tab).addClass('active');
	});
	
	
	jQuery('.addSession').click(function(){
		jQuery('.sessionForm').fadeIn();
		return false;
	});
	
	
	jQuery('.congSessions').hover(function(){
		jQuery('.congSessiondelete', this).addClass('active');
	}, function(){
		jQuery('.congSessiondelete', this).removeClass('active');
	});
        
        var voteToggle = "Cancel";
        jQuery('.addNewVote').click(function(){
            var voteTxt = jQuery(this).html();
            jQuery(this).html(voteToggle);
            voteToggle = voteTxt;
            jQuery('.addVote').slideToggle();
            return false;
        })
	
	
	
	jQuery('.voteAddForm').submit(function(){
		if (jQuery('.rc').val() == "") {
			jQuery('.rc').addClass('error');
			return false;
		}else{
			return true;
		}
	})
});


