
AEA Scorecard Plugin documentation and installation instructions

I. Installation

	To install the AEA Scorecard Plugin, please use the Github Updater Plugin (https://github.com/afragen/github-updater).  This will ensure that updates can be received an applied correctly.

	Follow the instructions to install via "manual upload" on the Github Updater Plugin link above.  Once installed, you can go to Settings->Github Updater  in the WP admin and select "Install Plugin."  

	Here are the fields to fill out on that page:

	Plugin URI: https://bitbucket.org/srgdev/15d21-aea-scorecard
	Remote Repository Host: Bitbucket
	Repository Branch: master

	Click on "Install Plugin" and the installation process will begin.  Once installed, you can navigate to "AEA Scorecard" at the bottom of the WP admin menu on the left side of the screen.

	For reference, here are database tables added by the scorecard plugin:

				(db prefix)scorecard_bills
				(db prefix)scorecard_sponsors
				(db prefix)scorecard_key_votes
				(db prefix)scorecard_votes
				(db prefix)scorecard_members
				(db prefix)scorecard_members_overwrite

II. Initial Setup

	When you select "AEA Scorecard" menu item you will be presented with a screen containing several tabs across the top.  To start with, we must first fill out some basic information:

	1. Select "Sessions of Congress" and then click the button "Add session." Add at least one session (i.e., 114) and the years that Congress was in session, then click "Save Changes."  These sessions can be selected to work on one at a time from the "Scorecard Settings" tab.  When you select a specific session, only bills, members, and votes for that session will appear on all plugin admin pages.  If you want to add a vote for 2014, make sure that 2014's congressional session is selected as the current session (see below).  All added sessions will appear on the front end in a drop down select box.  To remove sessions, hover over the session listing on this tab and click the red "x" that appears.  This will remove the session entry, but keep all data associated with that session.  If you add the session number back in, the data will re-appear once you select that session as the "current" session.

	2. Select "Scorecard Settings" and ensure that the current session you want to work on is selected.  Also, here you can determine a length for excerpts on Bill and Vote descriptions (number of characters to show).  This will affect the amount of text displayed in tables listing Bills and Votes.  Remember, the current session setting determines what members, votes, and bills will appear on the plugin admin pages.

	Click "Save Changes."

	3. Select "API Settings."  Here you will enter API keys so the plugin can communicated with the NYTimes and Sunlight APIs.  Use the following keys:

		NYTimes: e06ccbd8f28eb3edc212fb864b937517:15:64541977
		Sunlight: 2272bc4a597946d2b50c0c398c36799d

	Also, enter a Facebook App ID for sharing purposes.  This App ID should be the same one used on the rest of the site.  It appears that the current live site uses the following FB App ID:

		Facebook API Key: 196749420435782

	This App must be used on the appropriate URL, or it will cause an error.  On the testing site, we use our own App that is set for development.stoneridgegroup.com.

	Click "Save Changes" to update.

	4. Select "Site Content" to edit the plugin's text content areas.  Here are two text editor boxes.  The first box, "Home Page Intro," will appear when the user goes the Scorecard home page, next to the map and above the search.  The second box is content for the "About" page.  It will be called in using a shortcode (see below, "IV. Front End").  Finally, select the "About" page from the list of available pages.  This is so the Scorecard knows where to direct users who click on the "About" link.

	Click "Save Changes" when you are finished.

	5. Finally, select "Data Sync" to start the actual sync process.  Firstly, click "Sync Member Data."  This process can often take several minutes, and downloads all member information for the currently selected congressional session.  When it is complete, then click "Recalculate Scores" to udpate current scores.  You can repeat this process if data gets mismatched or outdated in any way.  Scores are calculated based on votes and bills, so without any votes and bills entered in the scores will be 0 for all members.  Return here to updated scores after adding votes and bills in.

III. Votes, Bills, and Members

	After you select the current session of congress you want to work on, you can now edit members, bills, and votes for this session.  To begin with, navigate to "Members" underneath the "AEA Scorecard" nav item.

	1. Members
	The members page has two main sections - Top Performers and Members Table.

		a. Top Performers
		You may select up to five top performers in either chamber to appear first in the "Top Performers" list on the scorecard home page.  Any unused entries will be auto-filled with members, ranked by score from high to low, in that chamber.

		b. Members Table
		You may view the list of members, or edit detailed member information from here.  When editing a member, remember to save your changes before navigating back to the members list.

	2. Votes
	The votes page lists currently added votes, and displays a button to add a new key vote.

		a. Adding a vote
		When you select "Add Key Vote" you are asked for the roll call number - this is the reference number used to pull vote information into the scorecard.  Examples would be h16-2015.  When you click "Add Vote," the scorecard searches for vote results and information, pulls them down, updates scores, and takes you to the edit vote page.

		b. Editing votes
		When you click "Add Vote" after entering roll call or click "Edit" on a current vote,  takes you to a page where you can add a custom title, description, html widgets, and select the AEA position on the right hand side.  Remember to save changes here.  Every save checks and calculates scores.

	3. Bills
	Similar to votes, you are presented with the same layout.  Adding a bill is the same as adding a vote, as well as editing.  Just remember that bill reference numbers differ in format to vote reference numbers.  Exmaple: hr1030-114.

IV. Front End Integration, Shortcodes

	To display the scorecard on the front end of the site, you must first create two pages dedicated to the scorecard - an "About" page and a "Scorecard" page, inside the WP admin panel.  The only content in these pages will be the shortcodes used to call in the scorecard information.  Shortcodes are codes inside square brackets that WP interprets as a special command:

		Home Page (Usually titled Scorecard): [score-card]
		About Page (titled About Scorecard): [score-card-about]

	Also, after you save these pages, make sure to add menu items so users can find these pages.  If you did not already select the "About Scorecard" page in the Scorecard settings, go there and select it now so that the scorecard knows where its own about page is.
