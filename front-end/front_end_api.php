<?php

include_once('front_end_model.php');
class front_end_api extends front_end_model
{

    function __construct($prefix){
        parent::__construct($prefix);
    }

    /**
    * compiles results for the home page
    *
    * @param $congress     string
    *
    * @return array
    */
    protected function home_results($congress){
        
        $data['topHouse'] = $this->getTopPerformers('house');
        $data['topSenate'] = $this->getTopPerformers('senate');
        $data['topHouseVotes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber = 'house' ORDER BY id DESC LIMIT 6");
        $data['topSenateVotes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber = 'senate' ORDER BY id DESC LIMIT 6");
        $data['membersArray'] = $this->buildMemberSuggestions($this->getAllResults('scorecard_members', "WHERE congress = '$congress'"));
        $data['intro'] = $this->getOption('homepage_intro');
        return $this->prepareDataOutput($data);
    }
    
    
    /**
    * compiles results for the Overall Results Page
    *
    * @param $congress     string
    *
    * @return array
    */
    protected function overall_results($congress){
        
        $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' ORDER BY lName ASC");
        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress'");
        return $this->prepareDataOutput($data);
        
    }

    /**
     * Results for House members, bills, and votes
     * @param $congress
     * @return array
     */
    protected function house_results($congress){
        $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND chamber='house' ORDER BY score DESC");
        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber='house'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress' AND chamber='house'");
        return $this->prepareDataOutput($data);
    }

    /**
     * Results for Senate members, bills, and votes
     * @param $congress
     * @return array
     */
    protected function senate_results($congress){
        $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND chamber='senate' ORDER BY score DESC");
        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber='senate'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress' AND chamber='senate'");
        return $this->prepareDataOutput($data);
    }


    /**
     * compiles results for the State Results Page
     *
     * @param $congress  string
     * @param $state     string
     *
     * @return array
     */
    protected function state_results($congress, $state, $party=false){
        
        $data['state'] = $state;
        if($party){
            $party = strtoupper($party);
            $state = strtoupper($state);
            $data['party'] = $party;
            $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND state = '$state' AND party='$party'");
        } else {
            $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND state = '$state'");
        }

        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress'");
        return $this->prepareDataOutput($data);
    
    }
    
    
    /**
    * compiles results for the Member Results Page
    *
    * @param $congress     string
    * @param $congID     string
    *
    * @return array
    */
    protected function member_results($congress, $congID){

        $data['member'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND congID = '$congID'");
        $data['votes'] = $this->getMemberVotes($data['member'], $congress);
        $data['bills'] = $this->getMemberBills($data['member'], $congress);
        $data['not_bills'] = $this->getMemberNotBills($data['member'], $congress);
        $data['houseAvg'] = $this->getOption('houseAvg');
        $data['senAvg'] = $this->getOption('senAvg');
        return $this->prepareDataOutput($data);
    
    }


    /**
     * compiles results for a single vote Page
     *
     * @param $congress string
     * @param $voteID string
     *
     * @return array
     */
    protected function vote_results($congress, $voteID){
        $data['vote'] = $this->getAllResults('scorecard_key_votes', "WHERE id = '$voteID'");
        $data['votes'] = $this->buildVoteMembers($data['vote'], $this->congress);
        return $this->prepareDataOutput($data);
    }


    /**
     * compiles results for a single Bill Page
     *
     * @param $congress string
     * @param $billID string
     *
     * @return array
     */
    protected function bill_results($congress, $billID){
        $data['bill'] = $this->getAllResults('scorecard_bills', "WHERE id = '$billID'");
        $data['members'] = $this->buildBillMembers($data['bill'], $congress);

        return $this->prepareDataOutput($data);
    }

    /**
     * compiles results for zip code search
     * @param $zip
     * @param $congress
     * @return array
     */
    protected function search_by_zip($zip, $congress){
        $data['zip'] = $zip;
        $data['members'] = $this->getMembersByZip($zip,$congress);

        return $this->prepareDataOutput($data);
    }

    /**
     * compiles results for name search
     * @param $name
     * @param $congress
     * @return array
     */
    protected function search_by_name($name, $congress){
        $data['name'] = $name;
        $data['members'] = $this->getMembersByName($name,$congress);

        return $this->prepareDataOutput($data);
    }

    protected function compare_results($congress){

        $compare_array = $_SESSION['compare'];

        $data['house'] = array('members' => array());
        $data['senate'] = array('members' => array());

        foreach($compare_array['house'] as $congID=>$member){
            $data['house']['members'][$congID] = array();
            $data['house']['members'][$congID]['member'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND congID = '$congID'");
            $data['house']['members'][$congID]['votes'] = $this->getMemberVotes($data['house']['members'][$congID]['member'], $congress);
            $data['house']['members'][$congID]['bills'] = $this->getMemberBills($data['house']['members'][$congID]['member'], $congress);

            foreach($data['house']['members'][$congID]['votes'] as $i=>$vote){
                $data['house']['members'][$congID]['votes'][$vote->voteID] = $vote;
                unset($data['house']['members'][$congID]['votes'][$i]);
            }

            foreach($data['house']['members'][$congID]['bills'] as $i=>$bill){
                $data['house']['members'][$congID]['bills'][$bill->bill_id] = $bill;
                unset($data['house']['members'][$congID]['bills'][$i]);
            }

            $data['house']['members'][$congID]['member'] = $data['house']['members'][$congID]['member'][0];
        }

        foreach($compare_array['senate'] as $congID=>$member){
            $data['senate']['members'][$congID] = array();
            $data['senate']['members'][$congID]['member'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND congID = '$congID'");
            $data['senate']['members'][$congID]['votes'] = $this->getMemberVotes($data['senate']['members'][$congID]['member'], $congress);
            $data['senate']['members'][$congID]['bills'] = $this->getMemberBills($data['house']['members'][$congID]['member'], $congress);

            foreach($data['senate']['members'][$congID]['votes'] as $i=>$vote){
                $data['senate']['members'][$congID]['votes'][$vote->voteID] = $vote;
                unset($data['senate']['members'][$congID]['votes'][$i]);
            }

            foreach($data['senate']['members'][$congID]['bills'] as $i=>$bill){
                $data['senate']['members'][$congID]['bills'][$bill->bill_id] = $bill;
                unset($data['senate']['members'][$congID]['bills'][$i]);
            }

            $data['senate']['members'][$congID]['member'] = $data['senate']['members'][$congID]['member'][0];
        }

        $data['house']['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber='house'");
        $data['senate']['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber='senate'");

        $data['house']['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress' AND chamber='house'");
        $data['senate']['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress' AND chamber='senate'");

        $data['house']['chamber'] = 'house';
        $data['senate']['chamber'] = 'senate';

        return $this->prepareDataOutput($data);

    }
}