<?php

/**
 * Class front_end_core
 *
 * Core class handling front end data querying and displaying
 *
 * Mostly general purpose functions and setup for WP DB, sessions, and DB query wrapper
 * methods
 */

class front_end_core
{
    
    // Wrapper for $wpdb
    private $db;

    private $optionPrefix;

    /**
     * Constructor
     * Assign global WP Database Object
     * Begins session for compare fn
     */
    public function __construct($prefix){

        // Get WP database as property
        global $wpdb;
        $this->db = $wpdb;

        // Start a new session for compare functionality
        $_SESSION['compare'] = !empty($_SESSION['compare']) ? $_SESSION['compare'] : array('house' => array(), 'senate' => array());

        // WP Options prefix
        $this->optionPrefix = $prefix;

    }

    /**
     * Adds member object to compare array, or false if 4 already in array
     * @param $member
     * @return bool
     */
    public function add_compare($member){
        if(count($_SESSION['compare'][$member->chamber]) < 4 && !isset($_SESSION['compare'][$member->chamber][$member->congID])){
            $_SESSION['compare'][$member->chamber][$member->congID] = $member;
            return true;
        } else {
            return false;
        }
    }

    /*
     * Remove an entry from the compare array, based on id and chamber
     * return void
     */
    public function remove_compare($member_id, $member_chamber){
        unset($_SESSION['compare'][$member_chamber][$member_id]);
    }

    /**
     * Sets current congress session for use in all queries and front end data presentation
     * @param mixed $session
     */
    public function set_congress_session($session = false){
        if($session){
            $_SESSION['session'] = $session;
        }
    }

    /**
     * Return currently selected session, or default value of 114
     * @return string
     */
    public function get_congress_session(){
        if(isset($_SESSION['session']) && is_string($_SESSION['session'])){
            return $_SESSION['session'];
        } else {
            $sessions = $this->getOption('sessions');
            $current = '114';
            foreach($sessions as $i=>$session){
                if($session['number'] > $current){
                    $current = $session['number'];
                }                
            }
            return $current;
        }
    }
    
    /**
    * Return array from DB with optional query modifers
    *
    * @param $table     string
    * @param $query     string
    *
    * @return array
    */
    public function getAllResults($table = "", $query = "") {
        if($table) {
            $tablename = $this->dbPrefix($table);
            return $this->db->get_results("SELECT * FROM $tablename $query");
        }else{
            die('getAllResults() $table can not be empty');
        }
    }


    /**
     * Return array from DB for given fields with optional query modifers
     *
     * @param  $table string
     * @param  $fields array
     * @param  $query  string
     *
     * @return mixed
     */
    public function getResults($table = "", $fields = array(), $query = ""){
        if( $table ) {
            $tablename = $this->dbPrefix($table);
            $fields_length = count($fields);
            if ($fields_length > 0) {
                $fieldString = "";
                $i = 1;
                foreach ($fields as $f) {
                    $fieldString .= $f;
                    if ($i < $fields_length) {
                        $fieldString .= ",";
                    }
                    $i++;
                }
                return $this->db->get_results("SELECT $fieldString FROM $tablename $query");
            } else {
                die('getResults() $fields array can not be empty');
            }
        }else{
            die('getResults() $table can not be empty');
        }
    }


    /**
     * return a query from the DB
     *
     * @param  $query  string
     * @return mixed
     */
    public function getQuery( $query = "" ){

        if( $query ){
            return $this->db->get_results($query);
        }else{
            die('getQuery() $query can not be empty');
        }
    }



    /**
    * Returns the prefixed table name
    *
    * @param $tbale  string
    *
    * @return string
    */
    public function dbPrefix($table){
        return $this->db->prefix . $table;
    }
    
    
    
    
    /**
     * A wrapper function delegating to WP get_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * 
     * @param $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param $default string default value to return if the option is not set
     * 
     * @return string the value from delegated call to get_option(), or optional default value
     * if option is not set.
     */
    public function getOption($optionName, $default = null) {
        $prefixedOptionName = $this->optionPrefix.$optionName;
        $retVal = get_option($prefixedOptionName);
        if (!$retVal && $default) {
            $retVal = $default;
        }
        return $retVal;
    }
    
    
    
     /**
     * Includes files from the /views folder, also passes in $data for filling
     * out views.  $view van be a single filename without the .php extension
     *
     * @param $view  string
     * @param $data  array
     */
    public function build($view = "", $data = array()){
        if($view) {
            $imgpath = $this->imagePath();
            include(plugin_dir_path(__FILE__) . '/views/' . $view . '.php');
        }else{
            die('build() $view can not be empty');
        }
    }
    
    
    /**
     * Includes files from the /views/parts folder, also passes in $data for filling
     * out views.
     * 
     * @param $view string
     * @param $data array
     *
     */
    function templatePart($view, $data = array()){
        $imgpath = $this->imagePath();
        include(plugin_dir_path(__FILE__)."/views/parts/".$view.".php");
    }

    /**
     * ruturns image path for templates
     *
     * @return string
     */
    private function imagePath(){
        return plugins_url('front-end/images/', dirname(__FILE__));
    }



    /**
     * Get current page url minus Query string
     *
     * @returns       string
     * 
     */
    function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
         $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        $pageURL = explode("?", $pageURL);
        return $pageURL[0];
    }


    /**
     * Generates site links
     *
     * @param $page     string
     * @param $string string
     *
     * @return string
     */
    public function getLink($page, $string = ""){
        $url = $this->curPageURL();
        return $url."?spage=".$page.$string;
    }


    /**
     * Changes date format
     *
     * @param $date  string
     * @param  $format  string
     *
     * @return string
     */
    public function changeDateFormat($date, $format = 'Y-m-d'){
        return date($format, strtotime($date));
    }

    /**
     * Returns full name of state
     *
     * @param $state
     * @return mixed
     */
    public function translateStateName($state){
        require('assets/states_array.php');
        $state = strtoupper($state);
        return $states[$state];
    }


    /**
     * Take the data need for the page and append data needed for every page
     *
     * @param  $data array  The $data array need for the page
     *
     * @return array
     */
    public function prepareDataOutput($data = array()){

        // Get all sessions, if exists
        if(!isset($data['sessions'])) {
            $data['sessions'] = $this->getOption('sessions');
            foreach($data['sessions'] as $i=>$session){
                $data['sessions'][$session['number']] = $session;
                unset($data['sessions'][$i]);
            }
        }

        // Always add comparison information to output data
        $data['compare'] = $_SESSION['compare'];

        // Add current session
        $data['current_session'] = $this->get_congress_session();

        // Last time data was updated
        $data['last_updated'] = $this->getOption('last_updated');

        // Lists of all congressional members by chamber
        $congress = $data['current_session'];

        $data['congress_members']['house'] = $this->getAllResults('scorecard_members', "WHERE chamber='house' AND congress='$congress' ORDER BY lName ASC");
        $data['congress_members']['senate'] = $this->getAllResults('scorecard_members', "WHERE chamber='senate' AND congress='$congress' ORDER BY lName ASC");

        return $data;
    }


    /**
     * Generate Excerpt of text
     *
     * @param  $text string
     * @param  $numb string
     *
     * @return string
     */
    public function exerpt($text = "", $numb = "80"){
        $text = stripslashes($text);
        if($this->getOption('excerpt_length')){$numb = $this->getOption('excerpt_length');}
        if (strlen($text) > $numb) {
            $text = substr($text, 0, $numb);
            $text = substr($text,0,strrpos($text," "));
            $etc = " ...";
            $text = $text.$etc;
        }
        return $text;
    }

    /**
     * Gets sharing link and text
     * @param $data
     * @return array
     */
    public function get_share_text($data){

        $text = '';
        $url = '';

        $path = @$_GET['spage'];
        if($path):

            if($path == 'senate'):
                $url = get_permalink() . '?spage=senate';
                $text = 'AEA Scorecard - Overall Senate Results';

                if(isset($_GET['sw'])):
                    $sw = $_GET['sw'];
                    if($sw == 'votes'):
                        $url =  get_permalink() . '?spage=senate&sw=votes';
                        $text = 'AEA Scorecard - Senate Votes';
                    elseif($sw == 'bills'):
                        $url =  get_permalink() . '?spage=senate&sw=bills';
                        $text = 'AEA Scorecard - Senate Bills';
                    endif;
                endif;

            elseif ($path == 'house'):
                $url = get_permalink() . '?spage=house';
                $text = 'AEA Scorecard - Overall House Results';

                if(isset($_GET['sw'])):
                    $sw = $_GET['sw'];
                    if($sw == 'votes'):
                        $url =  get_permalink() . '?spage=house&sw=votes';
                        $text = 'AEA Scorecard - House Votes';
                    elseif($sw == 'bills'):
                        $url =  get_permalink() . '?spage=house&sw=bills';
                        $text = 'AEA Scorecard - House Bills';
                    endif;
                endif;

            elseif ($path == 'overall'):
                $url = get_permalink() . '?spage=overall';
                $text = 'AEA Scorecard - Overall Results';

                if(isset($_GET['sw'])):
                    $sw = $_GET['sw'];
                    if($sw == 'votes'):
                        $url =  get_permalink() . '?spage=overall&sw=votes';
                        $text = 'AEA Scorecard - Overall Votes';
                    elseif($sw == 'bills'):
                        $url =  get_permalink() . '?spage=overall&sw=bills';
                        $text = 'AEA Scorecard - Overall Bills';
                    endif;
                endif;

            elseif($path == 'search'):
                $url = get_permalink() . '?' . $_SERVER['QUERY_STRING'];
                $text = 'AEA Scorecard - Search Results';

            elseif($path == 'vote'):
                $vote = $data['vote'][0];
                $url =  get_permalink() . '?spage=bill&id='.$vote->id;
                $text = 'AEA Scorecard  - '.$vote->vote_title;

            elseif($path == 'bill'):
                $bill = $data['bill'][0];
                $url =  get_permalink() . '?spage=bill&id='.$bill->id;
                $text = 'AEA Scorecard  - '.$bill->short_title;

            elseif($path == 'member'):

                $text = 'AEA Scorecard - ' . $data['member'][0]->title." ".$data['member'][0]->fName." ".$data['member'][0]->lName;
                $url =  get_permalink() . '?spage=state&state='.strtolower($data['member'][0]->state).'&pty='.strtolower($data['member'][0]->party);

            elseif($path == 'compare'):
                $url = get_permalink() . '?spage=compare';
                $text = 'AEA Scorecard - Comparison Page';

            elseif($path == 'state'):
                $url = get_permalink() . '?spage=state&state='.strtolower($data['state']);
                $text = 'AEA Scorecard - '.$this->translateStateName($data['state']). ' Results';
                if(isset($data['party'])):
                    $url = get_permalink() . '?spage=state&state='.strtolower($data['state']).'&pty='.$data['party'];
                    $text = 'AEA Scorecard - '.strtolower($data['party']) == 'r' ? 'Republican' : (strtolower($data['party']) == 'd' ? 'Democrat' : 'Independent').'s in '.$this->translateStateName($data['state']);
                endif;
            endif;
        endif;

        $text = urlencode($text);
        $url = urlencode($url);

        $url = urlencode('http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");

        return array('text' => $text, 'url' => $url);

    }
}