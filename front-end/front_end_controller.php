<?php


include_once('front_end_api.php');
class front_end_controller extends front_end_api
{

    /**
     * Current congress selection
     * @var bool
     */
    public $congress = false;

    /**
     * Constructor, self-assigns current congress
     */
    function __construct($prefix){
        parent::__construct($prefix);
        $this->congress = $this->get_congress_session();
    }

    /**
     * Main router for the frontEnd
     */
    public function siteRouter(){
        $page = (isset($_GET['spage'])) ? $_GET['spage'] : "";
        switch ($page) {
            case 'overall':
                $this->overall_page();
                break;
            case 'member':
                $this->member_page();
                break;
            case 'state':
                $this->state_page();
                break;
            case 'vote':
                $this->vote_page();
                break;
            case 'bill':
                $this->bill_page();
                break;
            case 'search':
                $this->search();
                break;
            case 'compare':
                $this->compare();
                break;
            case 'house':
                $this->house();
                break;
            case 'senate':
                $this->senate();
                break;
            default:
                $this->home_page();
        }
    }

    /**
     * Generate Home Page
     */
    public function home_page(){
        $data = $this->home_results($this->congress);
        $this->build('home_view',$data);
        
    }

    /**
     * Generate Overall Results Page
     */
    public function overall_page(){
        $data = $this->overall_results($this->congress);
        $this->build('overall_view', $data);
    }

    /**
     * Generate Member Page
     */
    public function member_page(){
        $congID = $_GET['id'];
        if($congID == 'undefined'){
            $data = false;
        } else {
            $data = $this->member_results($this->congress, $congID);
        }

        $this->build('member_view',$data);
    }

    /**
     * Generate State Page
     */
    public function state_page(){
        $state = $_GET['state'];
        $party = @$_GET['pty'];
        $data = $this->state_results($this->congress, $state, $party);
        $this->build('state_view', $data);
    }

    /**
     * Generate Single Vote Page
     */
    public function vote_page(){
        $voteID = $_GET['id'];
        $data = $this->vote_results($this->congress, $voteID);
        $this->build('vote_view', $data);
    }

    /**
     * Generate Single Bill Page
     */
    public function bill_page(){
        $billID = $_GET['id'];
        $data = $this->bill_results($this->congress, $billID);
        $this->build('bill_view', $data);
    }

    /**
     * Generate Search Page
     */
    public function search(){
        $zip = @$_GET['zip'];
        $name = @$_GET['leg_name'];

        if($zip){

            $data = $this->search_by_zip($zip, $this->congress);
            $data['query'] = $zip;
        } else if($name){
            $data = $this->search_by_name($name, $this->congress);
            $data['query'] = $name;
        }

        $this->build('search_view', $data);
    }

    /**
     * Generate Compare Page
     */
    public function compare(){
        $data = $this->compare_results($this->congress);
        $this->build('compare_view', $data);
    }

    public function house(){
        $data = $this->house_results($this->congress);
        $this->build('chamber_view', $data);
    }

    public function senate(){
        $data = $this->senate_results($this->congress);
        $this->build('chamber_view', $data);
    }
    
}