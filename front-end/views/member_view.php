<div id="scoreCardWrapper">
    <?php if($data): ?>
    <?php $this->templatePart("controls_row", $data); ?>
    <?php endif; ?>
	
	
	<div id="contentRow" class="row">
		<div class="rowInner">

        <?php if($data): ?>
            <div id="infoBar">
                <?php $this->templatePart('breadcrumbs', $data); ?>
                <?php $this->templatePart('share_panel', $data); ?>
                <br class="clear">
            </div> <!-- End infoBar -->


            <div id="repDetail">

                <div id="repPhoto">
                <div class="photo"><img src="<?php echo $data['member'][0]->image_path; ?>"></div>
                <div class="compare" data-id="<?php echo $data['member'][0]->congID; ?>" data-chamber="<?php echo $data['member'][0]->chamber; ?>"><i class="fa fa-check-circle-o"></i>&nbsp;&nbsp;Compare</div>
                </div>

                <div id="repInfo">
                <h3><?php echo $data['member'][0]->title." ".$data['member'][0]->fName." ".$data['member'][0]->lName; ?></h3>
                <p>(<?php echo $data['member'][0]->party; ?>) <?php echo $data['member'][0]->state; ?>&nbsp;&nbsp;&nbsp; <?php if($data['member'][0]->district) { echo "District ".$data['member'][0]->district; } ?></p>
                </div>



                <div id="repGraphs">
                <div class="currentScore barGraph">
                    <div class="graphFill" data-score='<?php echo $data['member'][0]->score; ?>'>
                    <div class="data vertMiddle"><div class="label">Current Score</div><div class="score"></div></div>
                    </div>
                </div>

                    <?php
                        $avg = ($data['member'][0]->chamber == 'house') ? $data['houseAvg'] : $data['senAvg'];
                    ?>
                <div class="avgScore barGraph">
                    <div class="graphFill" data-score='<?php echo $avg; ?>'>
                    <div class="data vertMiddle"><div class="label"><?php echo ($data['member'][0]->chamber == 'house') ? 'House' : 'Senate'; ?> Average</div><div class="score"></div></div>
                    </div>
                </div>
                </div>



                <br class="clear">
            </div>

            <ul class="tabsBar">
                <li class="active" data-table="votesTable"><div>Key Votes</div></li>
                <li data-table="billsTable"><div>Co-Sponsorships</div></li>
                <li data-table="notOnBillTable"><div>Did Not Co-Sponsor</div></li>
            </ul>

            <div class="votesTable overtable">
                <?php $this->templatePart("member_votes_table", $data['votes']); ?>
            </div>

            <div class="billsTable hide overtable">
                <?php $this->templatePart("member_bills_table", $data['bills']); ?>
            </div>

            <div class="notOnBillTable hide overtable">
                <?php $this->templatePart('member_bills_table', $data['not_bills']); ?>
            </div>

        <?php else :?>

            <div id="infoBar">
                <div id="infoPath">No results found</div>

                <br class="clear">
            </div> <!-- End infoBar -->

        <?php endif; ?>
		   
	    
	    </div> <!--END ROWINNER -->
	</div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>
	    
	
	<br class="clear">
</div>