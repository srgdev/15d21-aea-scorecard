<div id="scoreCardWrapper">
    <?php $vote = $data['vote'][0]; ?>
    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <?php $this->templatePart('breadcrumbs', $data); ?>
                <?php $this->templatePart('share_panel', $data); ?>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3><?php echo $vote->vote_title; ?> <br>Vote <?php echo $vote->roll_call; ?></h3>

            <div id="voteDetail">
                <div class="contentCol">
                    <h4>Description</h4>
                    <p><?php echo stripslashes($vote->description); ?></p>
                    <br>
                </div> <!-- End contentCol -->

                <div class="sideCol">
                    <div id="positionBlock">
                        <div class="position">AEA&nbsp;Position: <span><?php echo $vote->position; ?></span></div>
                        <div class="positionData">
                            <span>Vote result on <?php echo $this->changeDateFormat($vote->vote_date); ?></span>
                            <h3><?php echo $vote->result; ?></h3>
                            <?php echo $vote->count_yes; ?> to <?php echo $vote->count_no; ?>
                        </div>
                        <div class="widget-area">
                            <?php echo htmlspecialchars_decode($vote->widget); ?>
                        </div>

                    </div><!-- End positionBlock -->
                </div> <!-- End sideCol -->

                <br class="clear">
            </div>
            <div id="scrollPoint"></div>

            <ul class="tabsBar voteTabs">
                <li class="active yesVote"><div>VOTED YES</div></li>
                <li class="noVote"><div>VOTED NO</div></li>
                <li class="notVote"><div>DID NOT VOTE</div></li>
            </ul>

            <div class="scrollTitle">
                <h3><?php echo $vote->vote_title; ?></h3>
            </div>

            <?php $this->templatePart("vote_members_table", $data['votes']['members']); ?>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>