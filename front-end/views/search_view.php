<div id="scoreCardWrapper">

    <?php if($data['members']): ?>
        <?php $this->templatePart("controls_row", $data); ?>
    <?php endif; ?>


    <div id="contentRow" class="row">
        <div class="rowInner">

                <div id="infoBar">
                    <?php $this->templatePart('breadcrumbs', $data); ?>
                    <br class="clear">
                </div> <!-- End infoBar -->

            <h3>Search Results : <?php echo $data['query'] ?></h3>

            <ul class="tabsBar overallTabs">
                <li class="active" data-table="membersTable"><div>MEMBERS</div></li>
            </ul>

            <div class="scrollTitle">
                <h3>Overall Results</h3>
            </div>

            <div class="membersTable overtable">
                <?php $this->templatePart("members_table", $data['members']); ?>
            </div>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>