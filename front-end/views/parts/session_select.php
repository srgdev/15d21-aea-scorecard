<form class="searchForm selectForm">
    <select name="session" id="session" class="searchList">
        <?php foreach($data['sessions'] as $session): ?>
            <option value="<?php echo $session['number']; ?>" <?php echo $session['number'] == $data['current_session'] ? 'selected' : ''; ?>><?php echo $session['years']; ?></option>
        <?php endforeach; ?>
    </select>
</form>