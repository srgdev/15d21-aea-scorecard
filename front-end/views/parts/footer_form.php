<div id="subscribeRow" class="row">
    <div class="rowInner">
        <form method="post" action="http://t.lt02.net/q/IU6ZEWqJAYyGGV37aiN8feBu2HrPK_gfLg" data-validate="parsley">
            <label>Never miss a vote.</label>
            <input name="email" placeholder="Email" class="field" type="text" data-required="true" data-type="email">
            <input name="submit" type="submit" value="Sign Up" class="btn btnDarkBlue">
            <br class="clear">
        </form>

        <a class="aeaAboutLink" href="<?php echo get_permalink($this->getOption('about_page')); ?>">About AEA Scorecard</a>

    </div> <!--END ROWINNER -->
</div> <!-- END ROW -->

<?php if($this->getOption('take_action_link') != ''): ?>
<div id="takeActionRow" class="row">
    <div class="rowInner">

        <a class="takeActionLink btnDarkBlue" href="<?php echo get_permalink($this->getOption('take_action_link')); ?>">Take Action</a>

    </div> <!--END ROWINNER -->
</div> <!-- END ROW -->
<?php endif; ?>

<a href="#top" id="toTop"><i class="fa fa-arrow-circle-up"></i></a>