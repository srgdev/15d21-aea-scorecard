<div id="comparePanel">

    <div class="compareItems compareHouse" data-defaultimage="<?php echo $imgpath; ?>compare-h.jpg">
        <?php if(!empty($data['house'])): ?>
            <?php foreach($data['house'] as $member): ?>
                <div class="item active" data-member_id="<?php echo $member->congID; ?>"  style="background:url('<?php echo $member->image_path; ?>') no-repeat center; background-size: cover;"><div class="remove" data-id="<?php echo $member->congID; ?>" data-chamber="house">X</div></div>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php $diff = 4-(count($data['house'])); ?>
        <?php while($diff > 0): ?>
            <div class="item" style="background: url('<?php echo $imgpath; ?>compare-h.jpg') no-repeat center; background-size:cover;"><div class="remove">X</div></div>
            <?php $diff --; ?>
        <?php endwhile; ?>
        <br class="clear">
    </div>

    <a class="btn btnBlue" href="<?php the_permalink(); ?>?spage=compare"><div class="btnInner">COMPARE</div></a>

    <div class="compareItems compareSenate" data-defaultimage="<?php echo $imgpath; ?>compare-s.jpg">
        <?php if(!empty($data['senate'])): ?>
            <?php foreach($data['senate'] as $member): ?>
                <div class="item active" data-member_id="<?php echo $member->congID; ?>" style="background:url('<?php echo $member->image_path; ?>') no-repeat center; background-size: cover;"><div class="remove" data-id="<?php echo $member->congID; ?>" data-chamber="senate">X</div></div>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php $diff = 4-(count($data['senate'])); ?>
        <?php while($diff > 0): ?>
            <div class="item" style="background:url('<?php echo $imgpath; ?>compare-s.jpg') no-repeat center; background-size: cover;"><div class="remove">X</div></div>
            <?php $diff --; ?>
        <?php endwhile; ?>
        <br class="clear">
    </div>

</div>