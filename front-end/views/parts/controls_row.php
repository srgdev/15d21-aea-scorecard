<div id="controlsRow" class="row">
    <div class="rowInner">

        <div id="sessionPanel">
            <label>Last updated on <?php echo date('m/d/y', strtotime($data['last_updated'])); ?></label>
            <?php $this->templatePart("session_select", $data); ?>
        </div>

        <?php $this->templatePart("compare_panel", $data['compare']); ?>

        <br class="clear">

    </div> <!--END ROWINNER -->
</div> <!-- END ROW -->

<div id="houseList" class="membersList">
    <span class="closeButton"><i class="fa fa-times"></i></span>
    <h3>Add House Members to comparison</h3>
    <span>scroll to see full list</span>
    <?php $hm = $data['congress_members']['house']; ?>
    <form id="houseSelect">
        <?php foreach($hm as $member): ?>
            <div class="checkItem">

                <input <?php echo (isset($data['compare']['house'][$member->congID]) ? 'checked' : ''); ?> type="checkbox" value="<?php echo $member->congID; ?>" data-image="<?php echo $member->image_path; ?>" id="<?php echo $member->congID; ?>" />
                <label for="<?php echo $member->congID; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></label>
            </div>
        <?php endforeach; ?>
    </form>
    <a class="addButton" href="#">Add to list</a>
</div>

<div id="senateList" class="membersList">
    <span class="closeButton"><i class="fa fa-times"></i></span>
    <h3>Add Senate Members to comparison</h3>
    <span>scroll to see full list</span>
    <?php $hm = $data['congress_members']['senate']; ?>
    <form id="houseSelect">
        <?php foreach($hm as $member): ?>
            <div class="checkItem">

                <input <?php echo (isset($data['compare']['senate'][$member->congID]) ? 'checked' : ''); ?> type="checkbox" value="<?php echo $member->congID; ?>" data-image="<?php echo $member->image_path; ?>" id="<?php echo $member->congID; ?>" />
                <label for="<?php echo $member->congID; ?>"><?php echo $member->fName; ?> <?php echo $member->lName; ?></label>
            </div>
        <?php endforeach; ?>
    </form>
    <a class="addButton" href="#">Add to list</a>
</div>

