<?php $share_array = $this->get_share_text($data); ?>

<div id="infoSocial">
    <a href="#" onclick="window.print();return false;"><i class="fa fa-print"></i></a>
    <a href="mailto:?Subject=<?php echo urldecode($share_array['text']); ?>&body=<?php echo urldecode($share_array['text']);?>%0D%0A<?php echo ($share_array['url']);?>" target="_blank"><i class="fa fa-envelope"></i></a>
    <a href="#" data-id="<?php echo $this->getOption('fb_id'); ?>" data-text="<?php echo $share_array['text']; ?>" data-url="<?php echo $share_array['url']; ?>" id="fbSharingLink"><i class="fa fa-facebook"></i></a>
    <a href="https://twitter.com/intent/tweet?url=<?php echo $share_array['url']; ?>&text=<?php echo $share_array['text']; ?>&via=AEA" target="_blank"><i class="fa fa-twitter"></i></a>
</div>