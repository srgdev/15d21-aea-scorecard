
<?php $path = @$_GET['spage']; ?>

<div id="infoPath">
    <a href="<?php the_permalink(); ?>">Home</a>
    <i class="fa fa-angle-double-right"></i>
    <a href="<?php the_permalink(); ?>/?spage=overall"><?php echo $data['sessions'][$data['current_session']]['years']; ?></a>
    <?php if($path): ?>
        <i class="fa fa-angle-double-right"></i>

        <?php if($path == 'senate'): ?>
            <a href="<?php the_permalink(); ?>?spage=senate">Senate</a>

        <?php elseif ($path == 'house'): ?>
            <a href="<?php the_permalink(); ?>?spage=house">House</a>

        <?php elseif ($path == 'overall'): ?>
            <a href="<?php the_permalink(); ?>?spage=overall">Overall</a>

            <?php if(isset($_GET['sw'])): ?>
                <?php $sw = $_GET['sw']; ?>
                <?php if($sw == 'votes'): ?>
                    <i class="fa fa-angle-double-right"></i>
                    <a href="<?php the_permalink(); ?>?spage=overall&sw=votes">Votes</a>

                <?php elseif($sw == 'bills'): ?>
                    <i class="fa fa-angle-double-right"></i>
                    <a href="<?php the_permalink(); ?>?spage=overall&sw=bills">Bills</a>

                <?php endif; ?>
            <?php endif; ?>

        <?php elseif($path == 'search'): ?>
            <a href="<?php the_permalink(); ?>">Search</a>

        <?php elseif($path == 'vote'): ?>
            <?php $vote = $data['vote'][0]; ?>
            <?php if($vote->chamber == 'house'): ?>
                <a href="<?php the_permalink(); ?>?spage=house">House</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="<?php the_permalink(); ?>?spage=house&sw=votes">Votes</a>
            <?php elseif($vote->chamber == 'senate'): ?>
                <a href="<?php the_permalink(); ?>?spage=senate">Senate</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="<?php the_permalink(); ?>?spage=senate&sw=votes">Votes</a>
            <?php endif; ?>

        <?php elseif($path == 'bill'): ?>
            <?php $bill = $data['bill'][0]; ?>
            <?php if($bill->chamber == 'house'): ?>
                <a href="<?php the_permalink(); ?>?spage=house">House</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="<?php the_permalink(); ?>?spage=house&sw=bills">Bills</a>
            <?php elseif($bill->chamber == 'senate'): ?>
                <a href="<?php the_permalink(); ?>?spage=senate">Senate</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="<?php the_permalink(); ?>?spage=senate&sw=bills">Bills</a>
            <?php endif; ?>

        <?php elseif($path == 'member'): ?>

            <a href="<?php the_permalink(); ?>?spage=<?php echo $data['member'][0]->chamber; ?>"><?php echo ucFirst($data['member'][0]->chamber); ?></a>
            <i class="fa fa-angle-double-right"></i>
            <a href="<?php the_permalink(); ?>?spage=state&state=<?php echo strtolower($data['member'][0]->state); ?>"><?php echo $this->translateStateName($data['member'][0]->state); ?></a>
            <i class="fa fa-angle-double-right"></i>
            <a href="<?php the_permalink(); ?>?spage=state&state=<?php echo strtolower($data['member'][0]->state); ?>&pty=<?php echo strtolower($data['member'][0]->party); ?>"><?php echo strtolower($data['member'][0]->party) == 'r' ? 'Republican' : (strtolower($data['member'][0]->party) == 'd' ? 'Democrat' : 'Independent') ; ?></a>

        <?php elseif($path == 'compare'): ?>
            <a href="<?php the_permalink(); ?>?spage=compare">Compare</a>

         <?php elseif($path == 'state'): ?>
            <a href="<?php the_permalink(); ?>?spage=state&state=<?php echo strtolower($data['state']); ?>"><?php echo $this->translateStateName($data['state']); ?></a>
            <?php if(isset($data['party'])): ?>
                <i class="fa fa-angle-double-right"></i>
                <a href="<?php the_permalink(); ?>?spage=state&state=<?php echo strtolower($data['state']); ?>&pty=<?php echo $data['party']; ?>"><?php echo strtolower($data['party']) == 'r' ? 'Republican' : (strtolower($data['party']) == 'd' ? 'Democrat' : 'Independent') ; ?></a>
            <?php endif; ?>
        <?php endif; ?>

    <?php endif; ?>
</div>