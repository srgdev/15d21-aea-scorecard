<table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable compareTable">
    <tr class="photoRow">
        <td class=""></td>
        <td class="photoCell"><img src="<?php echo $imgpath; ?>compare-aea_logo.jpg"></td>

        <?php $members = @$data['members']; ?>
        <?php if(count($members) > 0) foreach($members as $i=>$member): ?>
            <?php $member = $member['member']; ?>
            <td class="photoCell">
                <a href="<?php the_permalink(); ?>?spage=member&id=<?php echo $member->congID; ?>"><div class="comparePhoto" style="background: url('<?php echo $member->image_path; ?>') no-repeat center; background-size: 102%;"></div></a>
            </td>
        <?php endforeach; ?>

        <?php $empty = (4-count($members)); while($empty > 0): ?>
            <td class="photoCell"><img class="addMember" data-type="<?php echo $data['chamber']; ?>" src="<?php echo $imgpath; ?>pic-compare.jpg"></td>
        <?php $empty--; endwhile; ?>


    </tr>

    <tr class="titleRow">
        <th class="titleCell">TITLE</th>
        <th class="aeaPosCell">AEA POSITION</th>

        <?php $members = @$data['members']; ?>
        <?php if(count($members) > 0) foreach($members as $i=>$member): ?>
            <?php $member = $member['member']; ?>
            <th class="nameCell"><?php echo $member->fName; ?> <?php echo $member->lName; ?></th>
        <?php endforeach; ?>

        <?php $empty = (4-count($members)); while($empty > 0): ?>
            <th class="nameCell">Add Member</th>
        <?php $empty--; endwhile; ?>

    </tr>
    <tr class="scoreRow">
        <td class=""></td>
        <td class=""></td>

        <?php $members = @$data['members']; ?>
        <?php if(count($members) > 0) foreach($members as $i=>$member): ?>
            <?php $member = $member['member']; ?>
            <td class="scoreCell"><?php echo $member->score; ?>%</td>
        <?php endforeach; ?>

        <?php $empty = (4-count($members)); while($empty > 0): ?>
            <td class="scoreCell">--</td>
        <?php $empty--; endwhile; ?>

    </tr>

    <?php $votes = @$data['votes']; ?>
    <?php foreach($votes as $i=>$vote): ?>
        <tr class="link" data-link="<?php echo $this->getLink('vote', "&id=".$vote->id); ?>">
            <td class="titleCell"><?php echo $vote->vote_title; ?></td>
            <?php if(strtolower($vote->position) == 'yes'): ?>
                <td class="aeaPosCell yesPos">YES</td>
            <?php else: ?>
                <td class="aeaPosCell noPos">NO</td>
            <?php endif; ?>

            <?php $members = @$data['members']; ?>

            <?php if(count($members) > 0) foreach($members as $i=>$member): ?>
                <?php if($member['votes'][$vote->id]->vote == 'Yes'): ?>
                    <td class="repPosCell yesPos">YES</td>
                <?php elseif($member['votes'][$vote->id]->vote == 'Not Voting'): ?>
                    <td class="repPosCell noPos">N/A</td>
                <?php else: ?>
                    <td class="repPosCell noPos">NO</td>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php $empty = (4-count($members)); while($empty > 0): ?>
                <td class="repPosCell"></td>
            <?php $empty--; endwhile; ?>

        </tr>
    <?php endforeach; ?>

</table>