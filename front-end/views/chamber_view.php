<div id="scoreCardWrapper">
    <?php $this->templatePart("controls_row", $data); ?>


    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <?php $this->templatePart('breadcrumbs', $data); ?>
                <?php $this->templatePart('share_panel', $data); ?>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3><?php echo ucfirst(htmlentities($_GET['spage'])); ?></h3>

            <ul class="tabsBar overallTabs">
                <li class="active" data-table="membersTable"><div>MEMBERS</div></li>
                <li data-table="votesTable"><div>VOTES</div></li>
                <li data-table="billsTable"><div>co-sponsorships</div></li>
            </ul>

            <div class="scrollTitle">
                <h3>Overall Results</h3>
            </div>

            <div class="membersTable overtable">
                <?php $this->templatePart("members_table", $data['members']); ?>
            </div>

            <div class="votesTable hide overtable">
                <?php $this->templatePart("votes_table", $data['votes']); ?>
            </div>

            <div class="billsTable hide overtable">
                <?php $this->templatePart("bills_table", $data['bills']); ?>
            </div>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>