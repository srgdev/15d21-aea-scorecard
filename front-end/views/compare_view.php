<div id="scoreCardWrapper">

    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <?php $this->templatePart('breadcrumbs', $data); ?>
                <?php $this->templatePart('share_panel', $data); ?>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3>Compare</h3>

            <div class="scrollTitle">
                <h3>Compare</h3>
            </div>

            <ul class="tabsBar compareTabs">
                <li data-type="house" class="<?php echo (isset($_GET['vc']) && $_GET['vc'] == 'house') ? 'active' : (!isset($_GET['vc']) ? 'active' : ''); ?> chamber"><div>House</div></li>
                <li data-type="senate" class="<?php echo (isset($_GET['vc']) && $_GET['vc'] == 'senate') ? 'active' : ''; ?> chamber"><div>Senate</div></li>
                <li data-type="votes" class="<?php echo (isset($_GET['vt']) && $_GET['vt'] == 'votes') ? 'active' : (!isset($_GET['vt']) ? 'active' : ''); ?> type"><div>Votes</div></li>
                <li data-type="sponsors" class="<?php echo (isset($_GET['vt']) && $_GET['vt'] == 'bills') ? 'active' : ''; ?> type"><div>Co-Sponsorships</div></li>
            </ul>

            <div class="houseTable_votes overtable <?php echo (isset($_GET['vc']) && $_GET['vc'] == 'house' && $_GET['vt'] == 'votes') ? '' : (!isset($_GET['vc']) ? '' : 'hide'); ?>">
                <?php $this->templatePart("compare_table_votes", $data['house']); ?>
            </div>

            <div class="senateTable_votes overtable <?php echo (isset($_GET['vc']) && $_GET['vc'] == 'senate' && $_GET['vt'] == 'votes') ? '' : 'hide'; ?>">
                <?php $this->templatePart("compare_table_votes", $data['senate']); ?>
            </div>

            <div class="houseTable_sponsors overtable <?php echo (isset($_GET['vc']) && $_GET['vc'] == 'house' && $_GET['vt'] == 'bills') ? '' : 'hide'; ?>">
                <?php $this->templatePart("compare_table_sponsors", $data['house']); ?>
            </div>

            <div class="senateTable_sponsors overtable <?php echo (isset($_GET['vc']) && $_GET['vc'] == 'senate' && $_GET['vt'] == 'bills') ? '' : 'hide'; ?>">
                <?php $this->templatePart("compare_table_sponsors", $data['senate']); ?>
            </div>


        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->

    <?php $this->templatePart("footer_form"); ?>

    <br class="clear">
</div>