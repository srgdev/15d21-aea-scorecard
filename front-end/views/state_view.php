<div id="scoreCardWrapper">

    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <?php $stateName = $this->translateStateName($data['state']); ?>

            <div id="infoBar">
                <?php $this->templatePart('breadcrumbs', $data); ?>
                <?php $this->templatePart('share_panel', $data); ?>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3>State Results: <span class="stateName blueTxt"><?php echo $stateName;  ?></span></h3>

            <div class="scrollTitle">
                <h3>State Results: <span class="stateName"><?php echo $stateName; ?></span></h3>
            </div>
            <?php $this->templatePart("members_table", $data['members']); ?>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->

    <?php $this->templatePart("footer_form"); ?>

    <br class="clear">
</div>