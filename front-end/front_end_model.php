<?php


include_once('front_end_core.php');
class front_end_model extends front_end_core
{

    function __construct($prefix){
        parent::__construct($prefix);
    }

    /**
     * Generates json for member name suggestions
     *
     * @param  $members array
     *
     * @return json
     */
    public function buildMemberSuggestions($members = array()){
        $membersArray = array();
        foreach($members as $m){
            $membersArray[$m->congID] = $m->fName." ".$m->lName." (".$m->state.")";
        }
        return json_encode($membersArray);
    }


    /**
     * Compile the members that are and are not on a given bill
     *
     * @param  $bill  array
     * @param  $congress  string
     *
     * @return mixed
     */
    public function buildBillMembers($bill = array(), $congress = ""){
        $chamber = $bill[0]->chamber;
        $billID = $bill[0]->bill_id;
        $members = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND chamber = '$chamber'");
        $sponsors = $this->getAllResults('scorecard_sponsors', "WHERE bill_id = '$billID'");
        $onBill = array();
        $notOnBill = array();
        foreach($members as $m) {
            $on_bill = false;
            foreach ($sponsors as $ob) {
                if($m->congID == $ob->member) {
                    array_push($onBill, $m);
                    $on_bill = true;
                }
            }
            if(!$on_bill){
                array_push($notOnBill, $m);
            }
        }
        $data['sponsorCount'] = count($sponsors);
        $data['onBill'] = $onBill;
        $data['notOnBill'] = $notOnBill;
        return $data;
    }


    /**
     * Compile list of Members for a given vote
     *
     * @param  $vote array
     * @param  $congress string
     *
     * @return mixed
     */
    public function buildVoteMembers( $vote = array(), $congress = "" ){
        $voteID = $vote[0]->roll_call;
        $memtable = $this->dbPrefix('scorecard_members');
        $votesTable = $this->dbPrefix('scorecard_votes');
        $data['members'] = $this->getQuery(
            "SELECT *
            FROM $memtable, $votesTable
            WHERE $votesTable.vote_id = '$voteID'
            AND $votesTable.member = $memtable.congID
            AND $memtable.congress = '$congress'
            ORDER BY $memtable.score DESC, $memtable.lName ASC");

        return $data;
    }


    /**
     * Compile votes for a given member
     *
     * @param  $member array
     * @param  $congress string
     *
     * @return mixed
     */
    public function getMemberVotes($member = array(), $congress = ""){
        $congID = $member[0]->congID;
        $chamber = $member[0]->chamber;
        $votesTable = $this->dbPrefix('scorecard_votes');
        $KeyVotesTable = $this->dbPrefix('scorecard_key_votes');

        $votes = $this->getQuery("
            SELECT *, $KeyVotesTable.id as voteID
            FROM $KeyVotesTable, $votesTable
            WHERE $KeyVotesTable.chamber = '$chamber'
            AND $KeyVotesTable.congress = '$congress'
            AND $votesTable.vote_id = $KeyVotesTable.roll_call
            AND $votesTable.member = '$congID'");

        return $votes;
    }

    /**
     * Returns an array of bills sponsored by a member during a given congress session
     * @param array $member
     * @param string $congress
     * @return mixed
     */
    public function getMemberBills($member = array(), $congress = ""){
        if(!empty($member)){
            $congID = $member[0]->congID;
            $billsTable = $this->dbPrefix('scorecard_bills');
            $sponsorsTable = $this->dbPrefix('scorecard_sponsors');

            $bills = array();

            $sponsored = $this->getQuery("SELECT bill_id FROM $sponsorsTable WHERE member='$congID' AND congress='$congress'");
            if(count($sponsored) > 0){
                foreach($sponsored as $sponsored_bill_id){
                $sponsored_bill_id = $sponsored_bill_id->bill_id;
                    $bill = $this->getQuery("SELECT * FROM $billsTable WHERE bill_id='$sponsored_bill_id'");

                    if(count($bill) > 0){
                        $bills[] = $bill[0];
                    }
                }
            }

            return $bills;
        } else {
            return array();
        }
        
    }

    /**
     * Returns an array of bills sponsored by a member during a given congress session
     * @param array $member
     * @param string $congress
     * @return mixed
     */
    public function getMemberNotBills($member = array(), $congress = ""){
        $congID = $member[0]->congID;
        $chamber = $member[0]->chamber;

        $billsTable = $this->dbPrefix('scorecard_bills');
        $sponsorsTable = $this->dbPrefix('scorecard_sponsors');

        $bills = array();

        $sponsored = $this->getQuery("SELECT bill_id FROM $sponsorsTable WHERE member='$congID' AND congress='$congress'");
        $sponsored_arr = array();
        foreach($sponsored as $bill){
            $sponsored_arr[$bill->bill_id] = $bill->bill_id;
        }

        $all_bills = $this->getQuery("SELECT * FROM $billsTable WHERE congress='$congress' AND chamber='$chamber'");

        foreach($all_bills as $bill){
            if(!isset($sponsored_arr[$bill->bill_id])){
                $bills[] = $bill;
            }
        }

        return $bills;
    }

    /**
     * runs a sunlight labs query by zip code
     *
     * @param $zip string
     * @param $congress string
     *
     * @return array
     */
    protected function getMembersByZip($zip, $congress){

        if (preg_match("/^\d{5}([\-]?\d{4})?$/i",$zip)){
            // Prep request
            $sunlightPath = "http://congress.api.sunlightfoundation.com/legislators/locate?zip=".$zip."&apikey=".$this->getOption('sunlightkey');

            // Get request
            $results = json_decode(file_get_contents($sunlightPath), true);

            // prepare a statement
            $members_query = "";

            // If there are results
            if(is_array($results['results']) && $results['count'] != 0){

                // Loop and prepare
                foreach($results['results'] as $r){
                    $members_query[] = "congID = '".$r['bioguide_id']."'";
                }

                // Join array
                $members_query = implode(' OR ', $members_query);

                // Query database and return
                $data =  $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND (".$members_query.")");

                return $data;

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * runs db query for name
     *
     * @param $name string
     * @param $congress string
     *
     * @return array
     */
    protected function getMembersByName($name, $congress){
        // Query database and return
        $data =  $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND (`fName` LIKE '%$name%' OR lName LIKE '%$name%')");
        return $data;
    }

    protected function getTopPerformers($chamber){

        // Get top options
        $top = $this->getOption('top_'.$chamber);

        // unset empties
        foreach($top as $i=>$member){
            if ($member == ''){
                unset($top[$i]);
            }
        }

        // Get counts
        $count_top = count($top);
        $filler = empty($top) ? 5 : 5 - $count_top;
        //$filler =  5 - $count_top;


        $top = implode(', ', $top);

        $top_members = $this->getAllResults('scorecard_members', 'WHERE id IN ('.$top.')');
        $top_left = $this->getAllResults('scorecard_members', 'WHERE chamber=\''.$chamber.'\' '.($filler == 5 ? '' : 'AND id NOT IN ('.$top.')').' ORDER BY LPAD(score, 3, \'0\') DESC LIMIT '.$filler);

        return array_merge($top_members, $top_left);

    }
}

