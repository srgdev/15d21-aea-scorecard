
    jQuery(document).ready(function ($) {

        // @TODO FINALE Document and organize this file
        //Run on Init
        setBtnInnerWidth();
        setEqualHeights();
        setGraphFill();
        // NEEDS TO RUN TWICE!!!
        setVertMiddle();
        setVertMiddle();


        //Run on Scroll or Resize
        jQuery(window).resize(function () {
            setBtnInnerWidth();
            setEqualHeights();
            setGraphFill();
            setVertMiddle();
        });
        jQuery(window).scroll(function () {
        });

        //Custom SelectBox
        jQuery('.searchList').Selectyze({
            theme: 'AEA'
        });

        jQuery('#autofill').val('');


        jQuery('tr.link').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).data('link');
            window.location = link;
        });

        jQuery("#toTop").scrollToTop();


        jQuery(".resultsTable").tablesorter({

            // define a custom text extraction function
            textExtraction: function(node) {

                // If name node
                if($(node).hasClass('nameCell')){

                    try {

                    // extract data from markup
                    var name = node.childNodes[0].data;

                    // Split into array
                    name = name.split(" ");

                    // Get last element in array (last name)
                    name = name[name.length - 1];

                    }
                    catch(e) {
                        var name = '';
                    }

                    // Return last name
                    return name;

                // Sort district numbers
                } else if ($(node).hasClass('districtCell')){

                    try {

                    // extract data from markup
                    var number = node.childNodes[0].data;

                    // Split into array
                    number = number.split(" ");

                    // Get last element in array (last name)
                    number = number[number.length - 1];

                    }
                    catch(e) {
                        var number = '';
                    }

                    // Return last name
                    return number;

                } else {
                    try {
                        var name = node.childNodes[0].data;
                    }
                    catch(e) {
                        var name = '';
                    }


                    return  name;
                }
            }
        });

        jQuery(".resultsTable").each(function(i, v){
            if($(v).height() > $(window).height()){
                $(v).stickyTableHeaders();
            }
        });

        jQuery(".tabsBar li").click(function () {
            jQuery('.overtable').addClass('hide');
            var table = jQuery(this).data('table');
            jQuery('.' + table).removeClass('hide');
            jQuery(".tabsBar li").removeClass('active');
            jQuery(this).addClass('active');
        });


        jQuery('.tip').click(function (e) {
            e.preventDefault();
            var url = jQuery(".mapLink").attr('rel') + jQuery(this).attr('rel');
            window.location = url;
        })


        jQuery('.nameSearch').submit(function (e) {
            //e.preventDefault();
            //window.location = jQuery(this).data('link') + jQuery('#autofill').data('congID');
            //return false;
        });

        jQuery('.zipsearch').submit(function (e) {
            e.preventDefault();
            window.location = jQuery(this).data('link') + jQuery('.zipfield').val();
            return false;
        });


        if(getParameterByName('sw') == 'votes'){
            jQuery('.overtable').addClass('hide');
            jQuery('.votesTable').removeClass('hide');
            jQuery(".tabsBar li").removeClass('active');
            jQuery("li[data-table='votesTable']").addClass('active');
        }

        if(getParameterByName('sw') == 'bills'){
            jQuery('.overtable').addClass('hide');
            jQuery('.billsTable').removeClass('hide');
            jQuery(".tabsBar li").removeClass('active');
            jQuery("li[data-table='billsTable']").addClass('active');
        }

        jQuery('.NoTR').hide();
        jQuery('.VotingTR').hide();
        jQuery('.PresentTR').hide();

        jQuery('.noVote').click(function(){

            jQuery('.YesTR').hide();
            jQuery('.VotingTR').hide();
            jQuery('.PresentTR').hide();
            jQuery('.NoTR').show();
            jQuery('html, body').animate({
                scrollTop: jQuery("#scrollPoint").offset().top - 50
            }, 500);
        });

        jQuery('.yesVote').click(function(){

            jQuery('.NoTR').hide();
            jQuery('.VotingTR').hide();
            jQuery('.PresentTR').hide();
            jQuery('.YesTR').show();
            if(jQuery('.tabsBar').hasClass('turn')) {
                jQuery('html, body').animate({
                    scrollTop: jQuery("#scrollPoint").offset().top - 50
                }, 500);
            }
        })

        jQuery('.notVote').click(function(){

            jQuery('.NoTR').hide();
            jQuery('.YesTR').hide();
            jQuery('.VotingTR').show();
            jQuery('.PresentTR').show();
            jQuery('html, body').animate({
                scrollTop: jQuery("#scrollPoint").offset().top - 50
            }, 500);
        })

        $('.compare').click(function(e){
            e.preventDefault();

            var photo = $('#repPhoto .photo img').attr('src');
            var id = $(this).data('id');
            var chamber = $(this).data('chamber');

            var wrap = $('.compareSenate');
            if(chamber == 'house'){
                var wrap = $('.compareHouse');
            }

            var req = $.get(aea_ajax_url + '?callback=?&action=get_add_compare&id=' + id, function(data){
                location.reload(true);
            });
            req.fail(function(data){
                if(data){
                    data = $.parseJSON(data.responseText);
                    alert(data.message);    
                } else {
                    location.reload(true);
                }
                
            })
        });

        $('.compareItems .remove').click(function(e){
            e.preventDefault();
            e.stopPropagation();

            var remove = $(this);
            var target = $(this).parent();
            var id = $(this).data('id');
            var chamber = $(this).data('chamber');
            var defaultImage = chamber == 'house' ? $('.compareHouse').data('defaultimage') : $('.compareSenate').data('defaultimage');
            var check = $('#'+id);

            var req = $.get(aea_ajax_url + '?callback=?&action=get_remove_compare&id=' + id + '&chamber=' + chamber, function(data){


                if(getParameterByName('spage') == 'compare'){
                    window.location.search = '?spage=compare' + currentTabsToQuery();
                } else {
                    location.reload(true);
                }

            });
            req.fail(function(data){
                data = $.parseJSON(data.responseText);
                alert(data.message);
            })


        });

        $('.item.active').click(function(e){
            window.location.search = '?spage=member&id='+$(this).data('member_id');
        })

        function currentTabsToQuery(){
            var currentChamber = $('.compareTabs li.chamber.active').data('type');
            var currentType = $('.compareTabs li.type.active').data('type');
            return '&vc='+currentChamber+'&vt='+currentType;
        }

        /**
         * Specialized tabs for compare table, multiple input options
         */
        $('.compareTabs li').unbind().click(function(e){

            // What has been clicked
            var selectedDataType = $(this).hasClass('type') ? 'type' : 'chamber';
            var selectedOption = $(this).data('type');

            // What's currently being shown
            var currentChamber = $(this).parent().find('.chamber.active').data('type');
            var currentType = $(this).parent().find('.type.active').data('type');

            // If choosing a chamber (house or senate)
            if(selectedDataType == 'chamber'){
                // If different than current chamber
                if(currentChamber != selectedOption){
                    $('li.chamber').toggleClass('active');
                    // Hide all tables
                    $('.overtable').addClass('hide');
                    // Show desired table
                    $('.'+selectedOption+'Table_'+currentType).removeClass('hide');
                }
            } else { // If choosing whether to view bills or co-sponsorships
                // If current view is different
                if(currentType != selectedOption){
                    $('li.type').toggleClass('active');
                    // Hide all tables
                    $('.overtable').addClass('hide');
                    // Show desired table
                    $('.'+currentChamber+'Table_'+selectedOption).removeClass('hide');
                }
            }
        });

        $('#session').change(function(e){
            var session = $(this).val();
            var req = $.get(aea_ajax_url + '?action=set_session&session='+session, function(){
                location.reload(true);
            })

            req.fail(function(data){
                data = $.parseJSON(data.responseText);
                alert(data.message);
            })

        });

        $('#fbSharingLink').click(function(e){
            e.preventDefault();

            var text = $(this).data('text');
            //var url = $(this).data('url');
            var url = window.location.href;
            var id = $(this).data('id');

            window.open('https://www.facebook.com/dialog/feed?app_id='+id+'&display=popup&caption='+text+'&link='+url+'&redirect_uri=https%3A%2F%2Fwww.facebook.com&name='+text, 'Share on FaceBook', 'width=640, height=400');
        });

        $('.closeButton').click(function(e){
            $('.membersList').slideUp(100);
        })

        $('.item').click(function(e){
            if(!$(this).hasClass('active')){
                var type = ($(this).parent().hasClass('compareHouse') ? 'house' : 'senate');
                $('#'+type+'List').slideDown(100);
            }
        });

        $('.addButton').click(function(e){
            $('.membersList').slideUp(100);
        })

        $('.addMember').click(function(e){
            var type = $(this).data('type');
            $('#'+type+'List').slideDown(100);
        })

        $('#houseList form input[type=checkbox]').change(function(e){
            if($('#houseList form input[type=checkbox]:checked').length > 4){
                $(this).attr('checked', false);
                alert('Please select no more than 4 members.');
            } else {
                if($(this).is(':checked')){
                    add_member($(this));
                } else {
                    remove_member($(this), 'house');
                }

            }
        })

        $('#senateList form input[type=checkbox]').change(function(e){
            if($('#senateList form input[type=checkbox]:checked').length > 4){
                $(this).attr('checked', false);
                alert('Please select no more than 4 members.');
            } else {
                if($(this).is(':checked')){
                    add_member($(this));
                } else {
                    remove_member($(this), 'senate');
                }
            }
        })

        function add_member($input){
            var id = $input.val();
            var req = $.get(aea_ajax_url + '?action=get_add_compare&id=' + id, function(){
                $('.addButton').unbind().click(function(e){
                    e.preventDefault();
                    $('.membersList').slideUp(100, function(){
                        if(getParameterByName('spage') == 'compare'){
                            window.location.search = '?spage=compare' + currentTabsToQuery();
                        } else {
                            location.reload(true);
                        }
                        console.log('got here');

                    })
                })
            })

            req.fail(function(data){
                data = $.parseJson(data);
                $input.attr('checked', false);
                alert(data.message);
            })
        }

        function remove_member($input, chamber){
            var id = $input.val();
            var req = $.get(aea_ajax_url + '?action=get_remove_compare&id=' + id + '&chamber=' + chamber, function(){
                $('.addButton').unbind().click(function(e){
                    e.preventDefault();
                    $('.membersList').slideUp(100, function(){
                        if(getParameterByName('spage') == 'compare'){
                            window.location.search = '?spage=compare' + currentTabsToQuery();
                        } else {
                            location.reload(true);
                        }
                    })
                })
            })

            req.fail(function(data){
                data = $.parseJson(data);
                $input.attr('checked', true);
                alert(data.message);
            })
        }

        $('#subscribeRow form').submit(function(){
            if($(this).parsley('validate')){
                window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
                this.target = 'formpopup';
            }
        })


    });


// Set Width of Button inner table cell
function setBtnInnerWidth() {
	jQuery( ".btn" ).each(function() {
		var btnWidth = jQuery(this).width()
		jQuery('.btnInner', this).width(btnWidth);
	});
};


function setEqualHeights() {
	jQuery('.setEqualHeights').each(function(){
		var currentTallest = 0;
		jQuery(this).children('div').css({'min-height': 0});
		jQuery(this).children('div').each(function(){
			if (jQuery(this).outerHeight() > currentTallest) { currentTallest = jQuery(this).outerHeight(); }
		});
		jQuery(this).children('div').css({'min-height': currentTallest});
	});
};

// Set Fill of bar graphs
function setGraphFill() {
	jQuery( ".graphFill" ).each(function() {
		var score = jQuery(this).attr('data-score');
		jQuery('.score', this).html(score +'%')
		if (jQuery(window).width() > 480 ) {
			jQuery(this).css({'width': score+'%'});
		}
		
		if (jQuery(this).outerWidth() <= 230) {
			jQuery('.score', this).css({'float': 'left'});
		} else {
			jQuery('.score', this).css({'float': 'right'});
		};
	});
};

// vertMiddle - Set vertical alignment to middle (can calculate the height for table-cell style vert middle or calculate for absolute style)
function setVertMiddle() {
	jQuery( ".vertMiddle" ).each(function() {
		var boxType = jQuery(this).css('display');
		if (boxType == 'table-cell') {
			var innerHeight = (jQuery(this).parent('.rowInner').css('min-height'));
			jQuery(this).css('height', innerHeight)
		}else{
			var vMargin = jQuery(this).height() / 2;
			jQuery(this).css('margin-top', '-' + vMargin + 'px').css('top', '50%').css('position', 'absolute');
		}
	});
}



function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}