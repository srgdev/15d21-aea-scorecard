<?php
/**
 * Define our http response code function if it does not exists
 */
if (!function_exists('http_response_code')) {
    function http_response_code($code = NULL) {

        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $code . ' ' . $text);

            $GLOBALS['http_response_code'] = $code;

        } else {

            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

        }

        return $code;

    }
}

// Depends on front end logic to parse data and process requests
include('front_end_controller.php');

/**
 * Class front_end_ajax
 *
 * Responds to GET requests using back end heavy lifting
 * Calls on front_end_controller for session and data parsing
 */

class front_end_ajax{

    // Placeholders for $_GET, class controller
    public $get;
    public $controller;

    function __construct(){

        // Include WP Core functions
        $this->load_wp_core();

        // Strip headers clean to prep for JSON response
        $this->set_headers();

        // Safely parse $_GET
        $this->get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        // Call a new instance of controller
        $this->controller = new front_end_controller('AeaScorecard_OptionsManager_');

        // Listen to $_GET
        $this->run();
    }

    /**
     * Strips output buffer to prepare for a clean request response, regardless of errors
     */
    private function set_headers(){
        if(ob_get_contents())
            ob_clean();
        header('Content-Type: application/json; charset=utf-8');
    }

    /**
     * Include necessary WP files and Deps to use
     * core wp functions inside controller class
     */
    private function load_wp_core(){

        function find_wordpress_base_path() {
            $dir = dirname(__FILE__);
            do {
                //it is possible to check for other files here
                if( file_exists($dir."/wp-config.php") ) {
                    return $dir;
                }
            } while( $dir = realpath("$dir/..") );
            return null;
        }

        define( 'BASE_PATH', find_wordpress_base_path()."/" );
        define('WP_USE_THEMES', false);
        global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
        require(BASE_PATH . 'wp-load.php');
    }

    /**
     * Main function that listens to $_GET and parses actions to self::methods
     */
    function run(){
        if($this->get['action']){
            $method = $this->get['action'];
            if(method_exists($this, $method)){
                $this->$method();
            } else {
                $this->respond('Invalid action/method.', 400);
            }
        }
    }

    /**
     * Adds a member, by ID, to the local compare session array
     */
    function get_add_compare(){
        if($this->get['id']){
            $congID = $this->get['id'];;
            $member = $this->controller->getAllResults('scorecard_members', "WHERE congID = '$congID'");
            if(count($member) > 0){
                if($this->controller->add_compare($member[0])){
                    $this->respond('Success', 200);
                } else {
                    $this->respond('Member already added, or you have already selected 4 Members.', 400);
                }
            } else {
                $this->respond('Member does not exist', 400);
            }
        } else {
            $this->respond('No member ID provided', 400);
        }
    }

    /**
     * Removes a member from local session array
     */
    function get_remove_compare(){
        if(isset($this->get['id']) && isset($this->get['chamber'])){
            $this->controller->remove_compare($this->get['id'], $this->get['chamber']);
            $this->respond('Removed', 200);
        } else {
            $this->respond('No member ID or Chamber provided', 400);
        }
    }

    /**
     * Sets HTTP headers and json-encodes response arrays and strings
     * @param $data mixed
     * @param $code int
     */
    function respond($data, $code){
        http_response_code($code);
        echo json_encode(array('message' => $data));
    }

    /**
     * Sets current congressional session from front end select input
     */
    function set_session(){
        if(isset($this->get['session']) && is_string($this->get['session'])){
            $this->controller->set_congress_session($this->get['session']);
        } else {
            $this->respond('No session provided', 400);
        }
    }

}

/**
 * Fire up the class and start processing requests
 */
$ajax = new front_end_ajax();